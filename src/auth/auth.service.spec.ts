import { JWTPayload } from './models/payload/jwt-payload';
import { TestingModule, Test } from '@nestjs/testing';
import { Repository } from 'typeorm';
import { User } from './../../src/database/entities/user.entity';
import { AuthService } from './../../src/auth/auth.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { UserLoginDTO } from './models/user/user-login.dto';
import { ForumSystemError } from '../common/exceptions/forum-system.error';

describe('AuthService should', () => {
  let jwtService;
  let service: AuthService;

  const userRepo: Partial<Repository<User>> = {
    findOne: jest.fn(),
  };
  beforeEach(async () => {
    jwtService = {
      signAsync: jest.fn(),
    };
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: getRepositoryToken(User),
          useValue: userRepo,
        },
        {
          provide: JwtService,
          useValue: jwtService,
        },
      ],
    }).compile();
    service = module.get<AuthService>(AuthService);
    jest.clearAllMocks();
  });

  it('be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findUserByName() should', () => {
    it('call userRepository.findOne() once with correct input data', async () => {
      // Arrange
      const name = 'username';
      const mockQuery = {
        where: {
          username: name,
          isDeleted: false,
        },
        relations: ['bans', 'roles'],
      };

      const spy = jest.spyOn(userRepo, 'findOne');

      // Act

      await service.findUserByName(name);
      // Assert

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(mockQuery);
    });

    it('return result of findOne() execution', async () => {
      // Arrange
      const name = 'username';
      const mockUser = new User();

      jest
        .spyOn(userRepo, 'findOne')
        .mockImplementation(() => Promise.resolve(mockUser));

      // Act
      const result = await service.findUserByName(name);

      // Assert

      expect(result).toEqual(mockUser);
    });
  });

  describe('validateUser() should', () => {
    it('call findUserByName() once with correct input data', async () => {
      // Arrange
      const name = 'username';
      const password = 'password';
      const mockUser = new User();
      const findUserByNameSpy = jest
        .spyOn(service, 'findUserByName')
        .mockImplementation(() => Promise.resolve(mockUser));

      jest
        .spyOn(bcrypt, 'compare')
        .mockImplementation(() => Promise.resolve(true));

      // Act
      await service.validateUser(name, password);

      // Assert
      expect(findUserByNameSpy).toBeCalledTimes(1);
      expect(findUserByNameSpy).toBeCalledWith(name);
    });

    it('return null if user is not found', async () => {
      // Arrange
      const name = 'username';
      const password = 'passowrd';
      jest
        .spyOn(service, 'findUserByName')
        .mockImplementation(() => Promise.resolve(undefined));

      // Act
      const result = await service.validateUser(name, password);

      // Assert
      expect(result).toEqual(null);
    });

    it('return user if found and validated', async () => {
      // Arrange
      const name = 'username';
      const password = 'passowrd';
      const mockUser = new User();
      jest.spyOn(service, 'findUserByName').mockResolvedValue(mockUser);
      jest.spyOn(bcrypt, 'compare').mockResolvedValue(true);

      // Act
      const result = await service.validateUser(name, password);

      // Assert
      expect(result).toEqual(mockUser);
    });

    it('return null if user is found, but not validated', async () => {
      // Arrange
      const name = 'username';
      const password = 'passowrd';
      const mockUser = new User();
      jest.spyOn(service, 'findUserByName').mockResolvedValue(mockUser);
      jest.spyOn(bcrypt, 'compare').mockResolvedValue(false);

      // Act
      const result = await service.validateUser(name, password);

      // Assert
      expect(result).toEqual(null);
    });
  });

  describe('login() should', () => {
    it('call validateUser() once with correct input data', async () => {
      // Arrange
      const mockUserCredentials: UserLoginDTO = {
        username: 'test',
        password: 'test',
      };
      const mockUserEntity = new User();
      mockUserEntity.roles = [{ id: 1, name: 'test' }];
      const mockToken = 'token';
      const validateUserSpy = jest
        .spyOn(service, 'validateUser')
        .mockResolvedValue(mockUserEntity);
      jest.spyOn(jwtService, 'signAsync').mockResolvedValue(mockToken);

      // Act
      await service.login(mockUserCredentials);

      // Assert
      expect(validateUserSpy).toBeCalledTimes(1);
      expect(validateUserSpy).toBeCalledWith(
        mockUserCredentials.username,
        mockUserCredentials.password,
      );
    });

    it('throw ForumSystemError if validateUser() returns a falsy value', async () => {
      // Arrange
      const mockUserCredentials: UserLoginDTO = {
        username: 'test',
        password: 'test',
      };
      const mockUserEntity = new User();
      mockUserEntity.roles = [{ id: 1, name: 'test' }];
      const mockToken = 'token';
      jest.spyOn(service, 'validateUser').mockResolvedValue(undefined);
      jest.spyOn(jwtService, 'signAsync').mockResolvedValue(mockToken);

      // Act & Assert
      expect(service.login(mockUserCredentials)).rejects.toThrowError(
        ForumSystemError,
      );
    });

    it('call signAsync() once with correct payload', async () => {
      // Arrange
      const mockUserCredentials: UserLoginDTO = {
        username: 'test',
        password: 'test',
      };
      const mockUserEntity = new User();
      mockUserEntity.roles = [{ id: 1, name: 'test' }];
      const mockPayload: JWTPayload = {
        id: mockUserEntity.id,
        username: mockUserEntity.username,
        bans: mockUserEntity.bans,
        roles: mockUserEntity.roles.map(el => el.name),
      };
      const token = 'token';
      jest.spyOn(service, 'validateUser').mockResolvedValue(mockUserEntity);
      const signAsyncSpy = jest
        .spyOn(jwtService, 'signAsync')
        .mockResolvedValue(token);

      // Act
      await service.login(mockUserCredentials);

      // Assert
      expect(signAsyncSpy).toBeCalledTimes(1);
      expect(signAsyncSpy).toBeCalledWith(mockPayload);
    });
    it('return generated token if user is found and validated', async () => {
      // Arrange
      const mockUserCredentials: UserLoginDTO = {
        username: 'test',
        password: 'test',
      };
      const mockUserEntity = new User();
      mockUserEntity.roles = [{ id: 1, name: 'test' }];
      const token = 'token';
      jest.spyOn(service, 'validateUser').mockResolvedValue(mockUserEntity);
      jest.spyOn(jwtService, 'signAsync').mockResolvedValue(token);

      // Act
      const result = await service.login(mockUserCredentials);

      // Assert
      expect(result).toEqual({ token });
    });
  });

  describe('logout() should', () => {
    it('return logout successful message', async () => {
      // Arrange
      const token = 'token';
      // Act
      const result = await service.logout(token);
      // Assert
      expect(result).toEqual({
        msg: 'Successful logout!',
      });
    });
  });
});
