import { Token } from './../common/decorators/token.decorator';
import { Controller, Post, Body, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';

@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  async login(@Body() user): Promise<{ token: string }> { 
    return await this.authService.login(user);
  }

  @Post('logout')
  @UseGuards(AuthGuard('jwt'))
  logout(@Token() token): Promise <{ msg: string }> {
    return this.authService.logout(token);
  }
}
