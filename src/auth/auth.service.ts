import { User } from './../../src/database/entities/user.entity';
import { ForumSystemError } from './../common/exceptions/forum-system.error';
import { Token } from './../common/decorators/token.decorator';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { UserLoginDTO } from './models/user/user-login.dto';
import { JWTPayload } from './models/payload/jwt-payload';

@Injectable()
export class AuthService {
  private readonly blacklist: string[] = [];

  public constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private readonly jwtService: JwtService,
  ) {}

  async findUserByName(name: string): Promise<User> {
    return await this.userRepository.findOne({
      where: {
        username: name,
        isDeleted: false,
      },
      relations: ['bans', 'roles'],
    });
  }

  async validateUser(name: string, password: string): Promise<User | null> {
    const user = await this.findUserByName(name);
    if (!user) {
      return null;
    }
    const isUserValidated = await bcrypt.compare(password, user.password);
    return isUserValidated ? user : null;
  }

  async login(loginUser: UserLoginDTO): Promise<{ token: string }> {
    const user = await this.validateUser(
      loginUser.username,
      loginUser.password,
    );

    if (!user) {
      throw new ForumSystemError('Wrong credentials!', 401);
    }

    if (user.bans) {
      throw new ForumSystemError('User Banned!', 401);
    }

    const payload: JWTPayload = {
      id: user.id,
      username: user.username,
      bans: user.bans,
      roles: user.roles.map(el => el.name),
    };

    const token = await this.jwtService.signAsync(payload);

    return {
      token,
    };
  }

  public async logout(@Token() token: string): Promise<{ msg: string }> {
    this.blacklist.push(token);
    return {
      msg: 'Successful logout!',
    };
  }

  public isTokenBlacklisted(@Token() token: string): boolean {
    return this.blacklist.includes(token);
  }
}
