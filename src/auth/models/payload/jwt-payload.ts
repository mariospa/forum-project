import { Ban } from './../../../database/entities/ban.entity';

export class JWTPayload {
    id: number;
    username: string;
    bans: Ban;
    roles: string[];
}
