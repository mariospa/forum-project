import { AuthService } from './../../client/src/app/services/auth.service';
import { TestingModule, Test } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { UserLoginDTO } from './models/user/user-login.dto';

describe('AuthController', () => {
  let authService;
  let controller: AuthController;
  beforeEach(async () => {
    authService = {
      login: jest.fn(),

      logout: jest.fn(),
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useValue: authService,
        },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    jest.clearAllMocks();
  });

  describe('login should', () => {
    it('call authService.login() once with correct user', async () => {
      // Arrange
      const spy = jest.spyOn(authService, 'login');

      const mockUser: UserLoginDTO = {
        username: 'username',
        password: 'password',
      };

      // Act

      await controller.login(mockUser);

      // Assert

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(mockUser);
    });

    it('return result from authService.login()', async () => {
      // Arrange
      const mockReturnValue = 'mock return value;';
      jest.spyOn(authService, 'login').mockReturnValue(mockReturnValue);
      const mockUser: UserLoginDTO = {
        username: 'username',
        password: 'password',
      };

      // Act

      const result = await controller.login(mockUser);

      // Assert

      expect(result).toEqual(mockReturnValue);
    });
  });

  describe('logout should', () => {
    it('call authService.logout() once with correct token', async () => {
      // Arrange
      const spy = jest.spyOn(authService, 'logout');
      const token = 'token';

      // Act

      await controller.logout(token);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(token);
    });

    it('return result from calling authService.logout()', async () => {
      // Arrange
      const mockReturnValue = 'mock return value;';
      jest.spyOn(authService, 'logout').mockReturnValue(mockReturnValue);
      const token = 'token';

      // Act

      const result = await controller.logout(token);

      // Assert
      expect(result).toEqual(mockReturnValue);
    });
  });
});
