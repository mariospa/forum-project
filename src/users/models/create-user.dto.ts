import { Length, Matches } from 'class-validator';

export class CreateUserDTO {
  @Length(2, 20, {
    message:
      'The username must be between 2 and 20 characters long.',
  })
  public username: string;

  @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/, {
    message:
      'The password must be minimum five characters, at least one letter and one number',
  })
  public password: string;
}