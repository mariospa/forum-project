import { User } from '../../database/entities/user.entity';
import { Expose, Transform } from 'class-transformer';
import { Posts } from '../../database/entities/posts.entity';

export class ShowUserDTO {
  @Expose()
  public id: number;

  @Expose()
  public username: string;

  @Expose()
  public avatarUrl: string;

  @Expose()
  @Transform((_, obj) => (obj as any).__posts__)
  public __posts__: Posts[];

  @Expose()
  @Transform((_, bans) => {
    if(bans.bans?.isBanned) {
      return bans.bans?.isBanned;
    } else {
      return false;
    }
  })
  public bans: boolean;

  @Expose()
  @Transform((_, obj) => obj.friends.map((x: any) => x.username))
  public friends: User[];

  @Expose()
  @Transform((_, obj) => obj.roles.map((x: any) => x.name))
  public roles: string[];
}