import { Expose, Transform } from 'class-transformer';

export class CreatedUserDTO {
  @Expose()
  public id: number;

  @Expose()
  public username: string;

  @Expose()
  public avatarUrl: string;

  @Expose()
  @Transform((_, obj) => obj.roles.map((x: any) => x.name))
  public roles: string[];
}
