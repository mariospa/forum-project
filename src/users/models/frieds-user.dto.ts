import { User } from './../../database/entities/user.entity';
import { Expose, Transform } from 'class-transformer';

export class FriendsUserDTO {
  @Expose()
  public id: number;

  @Expose()
  public username: string;

  @Expose()
  @Transform((_, obj) => obj.roles.map((x: any) => x.name))
  public roles: string[];

  @Expose()
  @Transform((_, obj) => obj.friends.map((x: any) => x.username))
  public friends: User[];
}
