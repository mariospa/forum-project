import { Expose, Transform } from 'class-transformer';
import { Posts } from '../../database/entities/posts.entity';

export class UserPostsDTO {
  @Expose()
  public id: number;

  @Expose()
  public username: string;

  @Expose()
  @Transform((_, obj) => (obj as any).__posts__.map((x: any) => x.title))
  public posts: Posts[];

}
