import { IsArray } from 'class-validator';
import { UserRole } from '../enums/user-role.enum';

export class UpdateUserRolesDTO {
  @IsArray()
  public roles: UserRole[];
}
