import { Length, IsOptional, Matches } from 'class-validator';

export class UpdateUserDTO {
  @IsOptional()
  @Length(2, 20)
  public username: string;

  @IsOptional()
  @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/, {
    message:
      'The password must be minimum five characters, at least one letter and one number',
  })
  public password: string;
}
