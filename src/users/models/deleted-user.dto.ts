import { Expose, Transform } from 'class-transformer';

export class DeletedUserDTO {
  @Expose()
  public id: number;

  @Expose()
  public username: string;

  @Expose()
  public isDeleted: boolean;

  @Expose()
  @Transform((_, obj) => obj.roles.map((x: any) => x.name))
  public roles: string[];
}
