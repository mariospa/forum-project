import { Ban } from './../database/entities/ban.entity';
import { FriendsUserDTO } from './models/frieds-user.dto';
import { DeletedUserDTO } from './models/deleted-user.dto';
import { CreatedUserDTO } from './models/created-user.dto';
import { UpdateUserRolesDTO } from './models/update-user-roles.dto';
import { ForumSystemError } from '../common/exceptions/forum-system.error';
import { CreateUserDTO } from './models/create-user.dto';
import { ShowUserDTO } from './models/show-user.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In, Like } from 'typeorm';
import { User } from '../database/entities/user.entity';
import { Role } from '../database/entities/role.entity';
import { UserRole } from './enums/user-role.enum';
import { hash } from 'bcrypt';
import { plainToClass } from 'class-transformer';
import * as fs from 'fs';
import * as path from 'path';

@Injectable()
export class UsersDataService {
    public constructor(
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Role) private readonly rolesRepository: Repository<Role>,
        @InjectRepository(Ban) private readonly bansRepository: Repository<Ban>,
    ) { }

    public async listAllUsers(name?: string) {
        const options = Object.create({});
        options.isDeleted = false;

        if (name) {
            options.username = Like(`%${name}%`);
        }

        const foundUsers = await this.usersRepository.find({
            where: options,
            relations: ['friends', 'bans']
        });

        if (foundUsers.length === 0) {
            throw new ForumSystemError(
                'No users found!',
                400,
            );
        }

        // return foundUsers

        return plainToClass(ShowUserDTO, foundUsers, {
            excludeExtraneousValues: true,
        });
    }

    public async findUserByUsername(name: string): Promise<ShowUserDTO[]> {
        const options = Object.create({});
        options.isDeleted = false;
        options.username = name;

        const foundUser = await this.usersRepository.find({
            where: options,
            relations: ['friends']
        });

        if (foundUser.length === 0) {
            throw new ForumSystemError(
                'No users found!',
                400,
            );
        }

        return plainToClass(ShowUserDTO, foundUser, {
            excludeExtraneousValues: true,
        });
    }

    public async findUserById(id: number): Promise<ShowUserDTO> {
        const foundUser = await this.usersRepository.findOne({
            where: { id, isDeleted: false },
            relations: ['friends', 'posts']
        });

        if (foundUser === undefined) {
            throw new ForumSystemError(
                'No user found!',
                400,
            );
        }

        return plainToClass(ShowUserDTO, foundUser, {
            excludeExtraneousValues: true,
        });
    }

    public async createUser(
        user: CreateUserDTO,
        ...roles: UserRole[]
    ): Promise<CreatedUserDTO> {
        const userRoles = roles.map(role => ({ name: role }));
        const roleEntities: Role[] = this.rolesRepository.create(
            userRoles,
        );

        const savedRoleEntities: Role[] = await this.rolesRepository.save(
            roleEntities,
        );

        const foundUser: User = await this.usersRepository.findOne({
            username: user.username,
        });

        if (foundUser) {
            throw new ForumSystemError(
                'User with such username already exists!',
                400,
            );
        }

        const userEntity: User = this.usersRepository.create(user);
        userEntity.roles = savedRoleEntities;
        userEntity.posts = Promise.resolve([]);
        userEntity.password = await hash(user.password, 10);

        const savedUser: User = await this.usersRepository.save(userEntity);

        return plainToClass(CreatedUserDTO, savedUser, {
            excludeExtraneousValues: true,
        });
    }

    public async updateUser(id: number, user): Promise<CreatedUserDTO> {
        const oldUser: User = await this.usersRepository.findOne({ id });

        if (oldUser === undefined || oldUser.isDeleted) {
            throw new ForumSystemError(
                'No user found!',
                400,
            );
        }

        // user.password = await hash(user.password, 10);
        const updatedUser = { ...oldUser, ...user };
        const savedUser = await this.usersRepository.save(updatedUser);

        return plainToClass(CreatedUserDTO, savedUser, {
            excludeExtraneousValues: true,
        });
    }

    public async deleteUser(id: number): Promise<DeletedUserDTO> {
        const foundUser = await this.usersRepository.findOne({ id });

        if (foundUser === undefined || foundUser.isDeleted) {
            throw new ForumSystemError(
                'No user found!',
                400,
            );
        }

        const deletedUser = await this.usersRepository.save({ ...foundUser, isDeleted: true });

        return plainToClass(DeletedUserDTO, deletedUser, {
            excludeExtraneousValues: true,
        });
    }

    public async updateUserRoles(
        id: number,
        updateUserRoles: UpdateUserRolesDTO,
    ): Promise<CreatedUserDTO> {
        const foundUser: User = await this.usersRepository.findOne({ id });
        if (foundUser === undefined || foundUser.isDeleted) {
            throw new ForumSystemError('No such user found', 404);
        }

        const roleEntities: Role[] = await this.rolesRepository.find({
            where: {
                name: In(updateUserRoles.roles),
            },
        });

        const updatedUser: User = { ...foundUser, roles: roleEntities };
        await this.usersRepository.save(updatedUser);

        return plainToClass(CreatedUserDTO, updatedUser, {
            excludeExtraneousValues: true,
        });
    }

    public async addFriends(id: number, user): Promise<FriendsUserDTO> {
        if (id === user.id) {
            throw new ForumSystemError('You cant befriend yourself!', 404);
        }
        const parentUser = await this.usersRepository.findOne({
            where: { id: user.id },
            relations: ['friends']
        });
        const childUser = await this.usersRepository.findOne({
            where: { id },
            relations: ['friends']
        });

        const friendsWithParent = childUser.friends.filter(el => el.id === user.id);
        const friendsWithChild = parentUser.friends.filter(el => el.id === id);

        if (friendsWithParent.length === 0) {
            childUser.friends.push(parentUser);
        } else {
            throw new ForumSystemError('you are already friends', 404);
        }

        if (friendsWithChild.length === 0) {
            parentUser.friends.push(childUser);
        } else {
            throw new ForumSystemError('you are already friends', 404);
        }

        const updatedUser = await this.usersRepository.save(parentUser);
        await this.usersRepository.save(childUser);

        return plainToClass(FriendsUserDTO, updatedUser, {
            excludeExtraneousValues: true,
        });
    }

    public async removeFriends(id: number, user): Promise<FriendsUserDTO> {
        if (id === user.id) {
            throw new ForumSystemError('You cant unfriend yourself!', 404);
        }
        const parentUser = await this.usersRepository.findOne({
            where: { id: user.id },
            relations: ['friends']
        });
        const childUser = await this.usersRepository.findOne({
            where: { id },
            relations: ['friends']
        });

        const friendsWithParent = childUser.friends.filter(el => el.id === user.id);
        const friendsWithChild = parentUser.friends.filter(el => el.id === id);

        if (friendsWithParent.length === 0) {
            throw new ForumSystemError('you are no friends anyway!', 404);
        } else {
            const friendsWithoutRemovedChild = childUser.friends.filter(el => el.id !== user.id);
            childUser.friends = friendsWithoutRemovedChild;
        }

        if (friendsWithChild.length === 0) {
            throw new ForumSystemError('are no friends anyway', 404);
        } else {
            const friendsWithoutRemovedParent = parentUser.friends.filter(el => el.id !== id);
            parentUser.friends = friendsWithoutRemovedParent;
        }

        const updatedUser = await this.usersRepository.save(parentUser);
        await this.usersRepository.save(childUser);

        return plainToClass(FriendsUserDTO, updatedUser, {
            excludeExtraneousValues: true,
        });
    }
    public async banUser(id: number, user): Promise<{ msg: string }> {
        if (id === user.id) {
            throw new ForumSystemError('You cannot ban yourself!', 404);
        }
        const foundUser = await this.usersRepository.findOne({
            where: { id },
            relations: ['bans']
        });

        const banEntity: Ban = this.bansRepository.create();
        banEntity.banReason = 'warum du Deppata!';
        banEntity.isBanned = true;

        await this.bansRepository.save(banEntity);

        foundUser.bans = banEntity;
        await this.usersRepository.save(foundUser);

        return {
            msg: 'user banned',
        };
    }

    public async unbanUser(id: number, user): Promise<{ msg: string }> {
        if (id === user.id) {
            throw new ForumSystemError('You cannot unban yourself!', 404);
        }

        const foundUser = await this.usersRepository.findOne({
            where: { id },
            relations: ['bans']
        });

        if (!foundUser) {
            throw new ForumSystemError('User doesnt exist!', 404);
        }

        if (foundUser.bans == null) {
            throw new ForumSystemError('User not banned anyway!', 404);
        }

        await this.bansRepository.delete({
            id: foundUser.bans.id
        });

        return {
            msg: 'user unbanned',
        };
    }

    public async deleteFile(image) {
        const t = fs.unlink(path.join(__dirname, '../../../', 'avatars/') + image,
        (err) => {
            if (err) {
                console.log(err);
            } else {
                console.log('file deleted');
            }
        });
    }
}
