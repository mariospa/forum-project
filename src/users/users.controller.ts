import { BanGuard } from './../common/guards/ban-guard';
import { FriendsUserDTO } from './models/frieds-user.dto';
import { DeletedUserDTO } from './models/deleted-user.dto';
import { UpdateUserDTO } from './models/update-user.dto';
import { CreatedUserDTO } from './models/created-user.dto';
import { AuthGuard } from '@nestjs/passport';
import { ForumSystemError } from '../common/exceptions/forum-system.error';
import { User } from './../common/decorators/user.decorator';
import { ShowUserDTO } from './models/show-user.dto';
import { UpdateUserRolesDTO } from './models/update-user-roles.dto';
import { AuthGuardWithBlacklisting } from './../common/guards/auth-guard-blacklisting';
import { CreateUserDTO } from './models/create-user.dto';
import { Controller, Post, Body, Get, HttpCode, HttpStatus, Query, Param, Put, Delete, UseGuards, UseInterceptors, UploadedFile, Res } from '@nestjs/common';
import { UsersDataService } from './users-data.service';
import { UserRole } from './enums/user-role.enum';
import { FileInterceptor } from '@nestjs/platform-express';
import { resolveSoa } from 'dns';

@Controller()
export class UsersController {
  public constructor(private readonly usersDataService: UsersDataService) { }

  @Get('users')
  @HttpCode(HttpStatus.OK)
  // @UseGuards(AuthGuard('jwt'), AuthGuardWithBlacklisting, BanGuard)
  public async allUsers(
    @Query('username') username: string
  ) {
    return await this.usersDataService.listAllUsers(username);
  }

  @Get('user/username')
  @HttpCode(HttpStatus.OK)
  public async findUserByUsername(
    @Query('username') username: string
  ): Promise<ShowUserDTO[]> {
    return await this.usersDataService.findUserByUsername(username);
  }

  @Get('users/:id')
  @HttpCode(HttpStatus.OK)
  public async getById(
    @Param('id') id: string
  ): Promise<ShowUserDTO> {
    return await this.usersDataService.findUserById(+id);
  }

  @Get('user')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), AuthGuardWithBlacklisting, BanGuard)
  public async getUser(
    @User() user
  ): Promise<ShowUserDTO> {
    return await this.usersDataService.findUserById(+user.id);
  }

  @Post('users')
  @HttpCode(HttpStatus.CREATED)
  public async addNewUser(
    @Body() user: CreateUserDTO
  ): Promise<CreatedUserDTO> {
    return await this.usersDataService.createUser(user, UserRole.Reporter);
  }

  @Put('users/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), AuthGuardWithBlacklisting)
  public async updateUser(
    @Param('id') id: string,
    @Body() body: UpdateUserDTO,
    @User() user,
  ): Promise<CreatedUserDTO> {
    if (user.id !== +id) {
      throw new ForumSystemError(
        'You dont have permission do update this user!',
        400,
      );
    }
    return await this.usersDataService.updateUser(+id, body);
  }

  @Delete('users/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), AuthGuardWithBlacklisting)
  public async deleteUser(
    @Param('id') id: string,
    @User() user,
  ): Promise<DeletedUserDTO> {
    if (user.id !== +id) {
      throw new ForumSystemError(
        'You dont have permission do delete this user!',
        400,
      );
    }
    return await this.usersDataService.deleteUser(+id);
  }

  @Put('user/:id/roles')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), AuthGuardWithBlacklisting)
  public async updateUserRoles(
    @User() user,
    @Param('id') id: string,
    @Body() updateUserRoles: UpdateUserRolesDTO,
  ): Promise<CreatedUserDTO> {
    if (user.id !== +id) {
      throw new ForumSystemError(
        'You dont have permission to edit the roles of this user!',
        400,
      );
    }
    return await this.usersDataService.updateUserRoles(+id, updateUserRoles);
  }

  @Post('user/:id/friends')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), AuthGuardWithBlacklisting)
  public async addFriends(
    @Param('id') userId: string,
    @User() user,
  ): Promise<FriendsUserDTO> {
    return await this.usersDataService.addFriends(+userId, user);
  }

  @Delete('user/:id/friends')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), AuthGuardWithBlacklisting)
  public async removeFriends(
    @Param('id') userId: string,
    @User() user,
  ): Promise<FriendsUserDTO> {
    return await this.usersDataService.removeFriends(+userId, user);
  }

  @Post('user/:id/ban')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), AuthGuardWithBlacklisting, BanGuard)
  public async banUser(
    @Param('id') userId: string,
    @User() user,
  ): Promise<{ msg: string }> {
    return await this.usersDataService.banUser(+userId, user);
  }

  @Delete('user/:id/ban')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), AuthGuardWithBlacklisting, BanGuard)
  public async unbanUser(
    @Param('id') userId: string,
    @User() user,
  ): Promise<{ msg: string }> {
    return await this.usersDataService.unbanUser(+userId, user);
  }

  @Post('avatar/:id')
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(FileInterceptor('file'))
  public async upload(
    @Param('id') userId: string,
    @UploadedFile() file: any,
    @Body('oldAvatarUrl') oldAvatarUrl?: string,
  ) {
    const updateUserProperties = {
      avatarUrl: `http://localhost:3000/avatar/` + file.filename,
    };

    if (oldAvatarUrl) {
      this.usersDataService.deleteFile(oldAvatarUrl.split('/').pop());
    }
    return await this.usersDataService.updateUser(+userId, updateUserProperties);
  }


  @Get('avatar/:imgpath')
  @HttpCode(HttpStatus.OK)
  public async getFile(
    @Param('imgpath') image,
    @Res() res,
  ) {
    return res.sendFile(image, { root: 'avatars' });
  }

  @Delete('avatar/:imgpath')
  @HttpCode(HttpStatus.OK)
  public async deleteFile(
    @Param('imgpath') image,
  ) {
    return this.usersDataService.deleteFile(image);
  }
}
