export enum UserRole {
    Reporter = 'Reporter',
    Moderator = 'Moderator',
    Admin = 'Admin',
    Basic = 'Basic',
  }
  