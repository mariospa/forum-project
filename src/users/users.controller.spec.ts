import { FriendsUserDTO } from './models/frieds-user.dto';
import { DeletedUserDTO } from './models/deleted-user.dto';
import { UpdateUserDTO } from './models/update-user.dto';
import { UpdateUserRolesDTO } from './models/update-user-roles.dto';
import { CreateUserDTO } from './models/create-user.dto';
import { UserRole } from './enums/user-role.enum';
import { CreatedUserDTO } from './models/created-user.dto';
import { ShowUserDTO } from './models/show-user.dto';
import { ForumSystemError } from '../common/exceptions/forum-system.error';
import { UsersController } from './users.controller';
import { UsersDataService } from './users-data.service';
import { Test, TestingModule } from '@nestjs/testing';

describe('Board Controller', () => {
  // global
  let controller: UsersController;
  const usersDataService: Partial<UsersDataService> = {
    listAllUsers() {
      return null;
    },
    findUserById() {
      return null;
    },
    updateUser() {
      return null;
    },
    deleteUser() {
      return null;
    },
    createUser() {
      return null;
    },
    updateUserRoles() {
      return null;
    },
    addFriends() {
      return null;
    },
    removeFriends() {
      return null;
    },
  };

  // beforeAll

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: UsersDataService, // token
          useValue: usersDataService,
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);

    jest.clearAllMocks();
  });

  // afterEach
  // afterAll

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('allUsers()', () => {
    it('should call listAllUsers with the username', async () => {
      // Arrange
      const user = 'username';
      const spy = jest.spyOn(usersDataService, 'listAllUsers');

      // Act
      await controller.allUsers(user);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(user);
    });

    it('should return listAllUsers and return mocked value', async () => {
      // Arrange
      const fakeUsername = 'username';
      const result = new ShowUserDTO();

      jest
        .spyOn(usersDataService, 'listAllUsers')
        .mockImplementation(() => Promise.resolve(result) as any);

      // Act
      const actualResult = await controller.allUsers(fakeUsername);

      // Assert
      expect(result).toEqual(actualResult);
    });
  });

  describe('getById()', () => {
    it('should call findUserById with id', async () => {
      // Arrange
      const user = '1';
      const spy = jest.spyOn(usersDataService, 'findUserById');

      // Act
      await controller.getById(user);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(1);
    });
    it('should return findUserById with mocked value', async () => {
      // Arrange
      const user = '1';
      const result = new ShowUserDTO();

      jest
        .spyOn(usersDataService, 'findUserById')
        .mockReturnValue(Promise.resolve(result));

      // Act
      const actualResult = await controller.getById(user);

      // Assert
      expect(result).toEqual(actualResult);
    });
  });

  describe('addNewUser()', () => {
    it('should call createUser', async () => {
      // Arrange
      const user = new CreateUserDTO();
      const role = UserRole.Reporter;
      const spy = jest.spyOn(usersDataService, 'createUser');

      // Act
      await controller.addNewUser(user);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(user, role);
    });
    it('should return findUserById with mocked value', async () => {
      // Arrange
      const user = new CreateUserDTO();
      const result = new CreatedUserDTO();

      jest
        .spyOn(usersDataService, 'createUser')
        .mockReturnValue(Promise.resolve(result));

      // Act
      const actualResult = await controller.addNewUser(user);

      // Assert
      expect(result).toEqual(actualResult);
    });
  });

  describe('updateUser()', () => {
    it('should call updateUser', async () => {
      // Arrange
      const id = '2';
      const body = new UpdateUserDTO();
      const user = { id: 2 };
      const spy = jest.spyOn(usersDataService, 'updateUser');

      // Act
      await controller.updateUser(id, body, user);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(2, body);
    });
    it('should throw if user id is not equal to id', async () => {
      // Arrange
      const id = '2';
      const body = new UpdateUserDTO();
      const user = { id: 1 };

      // Assert & Act
      expect(controller.updateUser(id, body, user)).rejects.toThrow(
        ForumSystemError,
      );
    });

    it('should return findUserById with mocked value', async () => {
      // Arrange
      const id = '2';
      const body = new UpdateUserDTO();
      const user = { id: 2 };
      const result = new CreatedUserDTO();

      jest
        .spyOn(usersDataService, 'updateUser')
        .mockReturnValue(Promise.resolve(result));

      // Act
      const actualResult = await controller.updateUser(id, body, user);

      // Assert
      expect(result).toEqual(actualResult);
    });
  });

  describe('deleteUser()', () => {
    it('should call createUser', async () => {
      // Arrange
      const id = '2';
      const user = { id: 2 };
      const spy = jest.spyOn(usersDataService, 'deleteUser');

      // Act
      await controller.deleteUser(id, user);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(2);
    });
    it('should throw if user id is not equal to id', async () => {
      // Arrange
      const id = '2';
      const user = { id: 1 };

      // Assert & Act
      expect(controller.deleteUser(id, user)).rejects.toThrow(ForumSystemError);
    });

    it('should return findUserById with mocked value', async () => {
      // Arrange
      const id = '2';
      const user = { id: 2 };
      const result = new DeletedUserDTO();

      jest
        .spyOn(usersDataService, 'deleteUser')
        .mockReturnValue(Promise.resolve(result));

      // Act
      const actualResult = await controller.deleteUser(id, user);

      // Assert
      expect(result).toEqual(actualResult);
    });
  });

  describe('updateUserRoles()', () => {
    it('should call updateUserRoles', async () => {
      // Arrange
      const user = { id: 2 };
      const id = '2';
      const mockRole = new UpdateUserRolesDTO();
      const spy = jest.spyOn(usersDataService, 'updateUserRoles');

      // Act
      await controller.updateUserRoles(user, id, mockRole);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(2, {});
    });
    it('should return findUserById with mocked value', async () => {
      // Arrange
      const user = { id: 2 };
      const id = '2';
      const mockRole = new UpdateUserRolesDTO();
      const result = new CreatedUserDTO();

      jest
        .spyOn(usersDataService, 'updateUserRoles')
        .mockReturnValue(Promise.resolve(result));

      // Act
      const actualResult = await controller.updateUserRoles(user, id, mockRole);

      // Assert
      expect(result).toEqual(actualResult);
    });
  });

  describe('deleteUser()', () => {
    it('should call deleteUser', async () => {
      // Arrange
      const id = '2';
      const user = { id: 2 };
      const spy = jest.spyOn(usersDataService, 'deleteUser');

      // Act
      await controller.deleteUser(id, user);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(2);
    });
    it('should throw if user id is not equal to id', async () => {
      // Arrange
      const id = '2';
      const user = { id: 1 };

      // Assert & Act
      expect(controller.deleteUser(id, user)).rejects.toThrow(ForumSystemError);
    });

    it('should return deleteUser with mocked value', async () => {
      // Arrange
      const id = '2';
      const user = { id: 2 };
      const result = new DeletedUserDTO();

      jest
        .spyOn(usersDataService, 'deleteUser')
        .mockReturnValue(Promise.resolve(result));

      // Act
      const actualResult = await controller.deleteUser(id, user);

      // Assert
      expect(result).toEqual(actualResult);
    });
  });

  describe('addFriends()', () => {
    it('should call addFriends', async () => {
      // Arrange
      const id = '2';
      const user = { id: 2 };
      const spy = jest.spyOn(usersDataService, 'addFriends');

      // Act
      await controller.addFriends(id, user);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(2, user);
    });

    it('should return addFriends with mocked value', async () => {
      // Arrange
      const id = '2';
      const user = { id: 2 };
      const result = new FriendsUserDTO();

      jest
        .spyOn(usersDataService, 'addFriends')
        .mockReturnValue(Promise.resolve(result) as any);

      // Act
      const actualResult = await controller.addFriends(id, user);

      // Assert
      expect(result).toEqual(actualResult);
    });
  });

  describe('removeFriends()', () => {
    it('should call removeFriends', async () => {
      // Arrange
      const id = '2';
      const user = { id: 2 };
      const spy = jest.spyOn(usersDataService, 'removeFriends');

      // Act
      await controller.removeFriends(id, user);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(2, user);
    });

    it('should return removeFriends with mocked value', async () => {
      // Arrange
      const id = '2';
      const user = { id: 2 };
      const result = new FriendsUserDTO();

      jest
        .spyOn(usersDataService, 'removeFriends')
        .mockReturnValue(Promise.resolve(result) as any);

      // Act
      const actualResult = await controller.removeFriends(id, user);

      // Assert
      expect(result).toEqual(actualResult);
    });
  });
});
