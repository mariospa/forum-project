import { Ban } from './../database/entities/ban.entity';
import { UpdateUserRolesDTO } from './models/update-user-roles.dto';
import { UserRole } from './enums/user-role.enum';
import { CreateUserDTO } from './models/create-user.dto';
import { ForumSystemError } from '../common/exceptions/forum-system.error';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from './../database/entities/user.entity';
import { Role } from './../database/entities/role.entity';
import { Repository, In } from 'typeorm';
import { UsersDataService } from './users-data.service';
import * as bcrypt from 'bcrypt';

describe('UsersService', () => {
    let service: UsersDataService;

    const usersRepo: Partial<Repository<User>> = {
        findOne() { return null; },
        find() { return null; },
        save() { return null; },
        create() { return null; },
    };
    const rolesRepo: Partial<Repository<Role>> = {
        findOne() { return null; },
        find() { return null; },
        save() { return null; },
        create() { return null; },
    };

    const banRepo: Partial<Repository<Ban>> = {
        findOne() { return null; },
        find() { return null; },
        save() { return null; },
        create() { return null; },
    };

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                UsersDataService,
                {
                    provide: getRepositoryToken(User),
                    useValue: usersRepo,
                },
                {
                    provide: getRepositoryToken(Role),
                    useValue: rolesRepo,
                },
                {
                    provide: getRepositoryToken(Ban),
                    useValue: banRepo,
                },
            ],
        }).compile();

        service = module.get<UsersDataService>(UsersDataService);

        jest.clearAllMocks();
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    describe('listAllUsers()', () => {
        it('listAllUsers should call find', async () => {
            // Arrange
            jest.spyOn(usersRepo, 'find');
            // Act
            try {
                await service.listAllUsers('testuser');
            } catch (e) {
                // empty
            }
            // Assert
            expect(usersRepo.find).toBeCalledTimes(1);
        });

        it('listAllUsers should throw if no users to show', async () => {
            // Arrange
            const returnValue = [];
            jest.spyOn(usersRepo, 'find').mockReturnValue(Promise.resolve(returnValue));
            // Act
            let error;
            try {
                await service.listAllUsers('testuser');
            } catch (e) {
                error = e;
            }
            // Assert
            expect(error).toBeInstanceOf(ForumSystemError);
            expect(error.message).toBe('No users found!');
        });
    });

    describe('findUserById() should', () => {
        it('call findOne with respective values', async () => {
            // Arrange
            const foundUser = new User();
            jest.spyOn(usersRepo, 'findOne').mockImplementation(() => Promise.resolve(foundUser));
            // Act
            try {
                await service.findUserById(1);
            } catch (e) {
                // empty
            }
            // Assert
            expect(usersRepo.findOne).toBeCalledTimes(1);
            expect(usersRepo.findOne).toBeCalledWith({
                where: { id: 1, isDeleted: false },
                relations: ['friends']
            });
        });

        it('findUserById should throw if no user found', async () => {
            // Arrange
            jest.spyOn(usersRepo, 'findOne').mockReturnValue(undefined);
            // Act
            let error;
            try {
                await service.findUserById(1);
            } catch (e) {
                error = e;
            }
            // Assert
            expect(error).toBeInstanceOf(ForumSystemError);
            expect(error.message).toBe('No user found!');
        });
    });

    describe('createUser() should', () => {
        it('call create with respective values', async () => {
            // Arrange
            const fakeUser = new CreateUserDTO();
            const fakeRoles = [UserRole.Reporter];
            const fakeUserRoles = new Role();
            const mappedRoles = fakeRoles.map(role => ({ name: role }));

            jest.spyOn(rolesRepo, 'create').mockReturnValue(fakeUserRoles);
            // Act
            try {
                await service.createUser(fakeUser, ...fakeRoles);
            } catch (e) {
                // empty
            }
            // Assert
            expect(rolesRepo.create).toBeCalledTimes(1);
            expect(rolesRepo.create).toBeCalledWith(mappedRoles);
        });

        it('call findOne with respective values', async () => {
            // Arrange
            const fakeUser = new CreateUserDTO();
            const fakeRoles = [UserRole.Reporter];

            jest.spyOn(usersRepo, 'findOne').mockReturnValue(undefined);
            // Act
            try {
                await service.createUser(fakeUser, ...fakeRoles);
            } catch (e) {
                // empty
            }
            // Assert
            expect(usersRepo.findOne).toBeCalledTimes(1);
            expect(usersRepo.findOne).toBeCalledWith({ username: fakeUser.username });
        });
        it('throw if user already exists', async () => {
            // Arrange
            const fakeUser = new CreateUserDTO();
            const fakeRoles = [UserRole.Reporter];
            const user = new User();

            jest.spyOn(usersRepo, 'findOne').mockReturnValue(Promise.resolve(user));
            // Act & Assert
            expect(service.createUser(fakeUser, ...fakeRoles)).rejects.toThrow(ForumSystemError);
        });

        it('call save with respective values', async () => {
            // Arrange
            const fakeUser = new CreateUserDTO();
            const fakeRoles = [UserRole.Reporter];
            const fakeRolesArr = new Role();

            jest.spyOn(rolesRepo, 'save').mockReturnValue(Promise.resolve(fakeRolesArr));
            // Act
            try {
                await service.createUser(fakeUser, ...fakeRoles);
            } catch (e) {
                // empty
            }
            // Assert
            expect(rolesRepo.save).toBeCalledTimes(1);
            expect(rolesRepo.save).toBeCalledWith({ username: fakeUser.username });
        });
    });

    describe('updateUser() should', () => {
        it('call findOne with respective values', async () => {
            // Arrange
            const id = 1;
            const fakeUser = new CreateUserDTO();

            jest.spyOn(usersRepo, 'findOne').mockReturnValue(undefined);
            // Act
            try {
                await service.updateUser(id, fakeUser);
            } catch (e) {
                // empty
            }
            // Assert
            expect(usersRepo.findOne).toBeCalledTimes(1);
            expect(usersRepo.findOne).toBeCalledWith({ id });
        });
        it('throw if user doesnt exist', async () => {
            // Arrange
            const id = 1;
            const fakeUser = new CreateUserDTO();

            jest.spyOn(usersRepo, 'findOne').mockReturnValue(undefined);
            // Act & Assert
            expect(service.updateUser(id, fakeUser)).rejects.toThrow(ForumSystemError);
        });

        it('call save with respective values', async () => {
            // Arrange
            const id = 1;
            const fakeUser = new CreateUserDTO();
            const returnUser = new User();

            jest.spyOn(usersRepo, 'findOne').mockReturnValue(Promise.resolve(returnUser));
            jest.spyOn(usersRepo, 'save').mockReturnValue(undefined);
            jest.spyOn(bcrypt, 'hash').mockReturnValue(undefined);
            // Act
            try {
                await service.updateUser(id, fakeUser);
            } catch (e) {
                // empty
            }
            // Assert
            expect(usersRepo.save).toBeCalledTimes(1);
            expect(usersRepo.save).toBeCalledWith({ ...returnUser, ...fakeUser });
        });
    });

    describe('deleteUser() should', () => {
        it('call findOne with respective values', async () => {
            // Arrange
            const id = 1;
            const fakeUser = new User();

            jest.spyOn(usersRepo, 'findOne').mockReturnValue(Promise.resolve(fakeUser));
            // Act
            try {
                await service.deleteUser(id);
            } catch (e) {
                // empty
            }
            // Assert
            expect(usersRepo.findOne).toBeCalledTimes(1);
            expect(usersRepo.findOne).toBeCalledWith({ id });
        });

        it('throw if user doesnt exist', async () => {
            // Arrange
            const id = 1;
            jest.spyOn(usersRepo, 'findOne').mockReturnValue(undefined);
            // Act & Assert
            expect(service.deleteUser(id)).rejects.toThrow(ForumSystemError);
        });
    });

    describe('updateUserRoles() should', () => {
        it('call findOne with respective values', async () => {
            // Arrange
            const id = 1;
            const fakeUserRoles = new UpdateUserRolesDTO();

            jest.spyOn(usersRepo, 'findOne').mockReturnValue(Promise.resolve(new User()));
            // Act
            try {
                await service.updateUserRoles(id, fakeUserRoles);
            } catch (e) {
                // empty
            }
            // Assert
            expect(usersRepo.findOne).toBeCalledTimes(1);
            expect(usersRepo.findOne).toBeCalledWith({ id });
        });

        it('throw if user doesnt exist', async () => {
            // Arrange
            const id = 1;
            const fakeUserRoles = new UpdateUserRolesDTO();

            jest.spyOn(usersRepo, 'findOne').mockReturnValue(undefined);
            // Act & Assert
            expect(service.updateUserRoles(id, fakeUserRoles)).rejects.toThrow(ForumSystemError);
        });

        it('call find with respective values', async () => {
            // Arrange
            const id = 1;
            const fakeUserRoles = new UpdateUserRolesDTO();

            jest.spyOn(usersRepo, 'findOne').mockReturnValue(Promise.resolve(new User()));
            jest.spyOn(rolesRepo, 'find').mockReturnValue(undefined);
            // Act
            try {
                await service.updateUserRoles(id, fakeUserRoles);
            } catch (e) {
                // empty
            }
            // Act & Assert
            expect(rolesRepo.find).toBeCalledTimes(1);
            expect(rolesRepo.find).toBeCalledWith({
                where: {
                    name: In(fakeUserRoles.roles),
                },
            });
        });
        it('call save with respective values', async () => {
            // Arrange
            const id = 1;
            const fakeUser = new User();
            const fakeUserRoles = new UpdateUserRolesDTO();
            const fakeRoleEntities = [new Role()];

            jest.spyOn(usersRepo, 'findOne').mockReturnValue(Promise.resolve(fakeUser));
            jest.spyOn(rolesRepo, 'find').mockReturnValue(Promise.resolve(fakeRoleEntities));
            jest.spyOn(usersRepo, 'save').mockReturnValue(undefined);
            // Act
            try {
                await service.updateUserRoles(id, fakeUserRoles);
            } catch (e) {
                // empty
            }
            // Act & Assert
            expect(usersRepo.save).toBeCalledTimes(1);
            expect(usersRepo.save).toBeCalledWith({ ...fakeUser, roles: fakeRoleEntities });
        });
    });

    describe('addFriends() should', () => {
        it('call findOne with respective values', async () => {
            // Arrange
            const id = 1;
            const user = {id:2, name: 'me'};

            const spy = jest.spyOn(usersRepo, 'findOne').mockReturnValue(Promise.resolve(new User()));
            // Act
            try {
                await service.addFriends(id, user);
            } catch (e) {
                // empty
            }
            // Assert
            expect(spy).toBeCalledTimes(2);
            expect(spy.mock.calls[0][0]).toEqual({'relations': ['friends'], 'where': {'id': 2}});
            expect(spy.mock.calls[1][0]).toEqual({'relations': ['friends'], 'where': {'id': 1}});
        });
        it('throw if user.id and id are the same', async () => {
            const id = 1;
            const user = {id:2, name: 'me'};
            // Act
            expect(service.addFriends(id, user)).rejects.toThrow(ForumSystemError);
        });
    });
    describe('removeFriends() should', () => {
        it('call findOne with respective values', async () => {
            // Arrange
            const id = 1;
            const user = {id:2, name: 'me'};

            const spy = jest.spyOn(usersRepo, 'findOne').mockReturnValue(Promise.resolve(new User()));
            // Act
            try {
                await service.removeFriends(id, user);
            } catch (e) {
                // empty
            }
            // Assert
            expect(spy).toBeCalledTimes(2);
            expect(spy.mock.calls[0][0]).toEqual({'relations': ['friends'], 'where': {'id': 2}});
            expect(spy.mock.calls[1][0]).toEqual({'relations': ['friends'], 'where': {'id': 1}});
        });
        it('throw if user.id and id are the same', async () => {
            const id = 1;
            const user = {id:2, name: 'me'};
            // Act
            expect(service.removeFriends(id, user)).rejects.toThrow(ForumSystemError);
        });
    });
});