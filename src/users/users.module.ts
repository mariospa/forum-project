import { ForumSystemError } from './../common/exceptions/forum-system.error';
import { Ban } from './../database/entities/ban.entity';
import { Module } from '@nestjs/common';
import { UsersDataService } from './users-data.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { Role } from '../database/entities/role.entity';
import { UsersController } from './users.controller';
import { MulterModule } from '@nestjs/platform-express';
import { extname } from 'path';
import { diskStorage } from 'multer';


@Module({
  imports: [
    TypeOrmModule.forFeature([User, Role, Ban]),
    MulterModule.register({
      fileFilter(_, file, cb) {
        const ext = extname(file.originalname);
        const allowedExtensions = ['.png', '.jpg', '.gif', '.jpeg'];

        if (!allowedExtensions.includes(ext)) {
          return cb(
            new ForumSystemError('Only images are allowed', 400),
            false,
          );
        }

        cb(null, true);
      },
      storage: diskStorage({
        destination: './avatars',
        filename: (_, file, cb) => {
          const randomName = Array.from({ length: 32 })
            .map(() => Math.round(Math.random() * 10))
            .join('');

          return cb(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
    }),
  ],
  controllers: [UsersController],
  providers: [UsersDataService],
})
export class UsersModule {}