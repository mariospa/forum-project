import { Comments } from '../database/entities/comments.entity';
import { CreateCommentDTO } from './models/create-comment.dto';
import {
  Injectable,
  NotFoundException,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Repository, Like } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CommentDTO } from './models/comment.dto';
import { UpdateCommentDTO } from './models/update-comment.dto';
import { Posts } from '../database/entities/posts.entity';
import { User } from '../database/entities/user.entity';
import { plainToClass } from 'class-transformer';
import { Vote } from './../database/entities/vote.entity';
import { ForumSystemError } from '../common/exceptions/forum-system.error';

@Injectable()
export class CommentsDataService {
  public constructor(
    @InjectRepository(Posts)
    private readonly postsRepository: Repository<Posts>,
    @InjectRepository(Comments)
    private readonly commentsRepository: Repository<Comments>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Vote)
    private readonly votesRepository: Repository<Vote>,
  ) {}

  public async findAllCommentsForPost(
    postId: number,
    next = 0,
    limit = 10,
  ): Promise<CommentDTO[]> {
    const findCommentsForPost: Comments[] = await this.commentsRepository.find({
      where: {
        post: postId,
        isDeleted: false,
      },
      skip: next,
      take: limit,
    });

    if (!findCommentsForPost.length) {
      return { message: 'This post has no comments yet!' } as any;
    }

    return plainToClass(CommentDTO, findCommentsForPost, {
      excludeExtraneousValues: true,
    });
  }

  public async commentsCount(postId: number, deleted = false): Promise<number> {
    return await this.commentsRepository.count({
      where: {
        post: postId,
        isDeleted: deleted,
      },
    });
  }

  public async queryComments(
    content: string,
    author?: string,
  ): Promise<CommentDTO[]> {
    const queryComments: Comments[] = author
      ? await this.commentsRepository.find({
          where: {
            title: Like(`%${content.toLowerCase()}`),
            content: Like(`%${content.toLowerCase()}`),
            isDeleted: false,
          },
        })
      : await this.commentsRepository.find({
          where: {
            title: Like(`%${content.toLowerCase()}`),
            content: Like(`%${content.toLowerCase()}`),
            user: author,
            isDeleted: false,
          },
        });

    if (!queryComments) {
      throw new HttpException('Not Found', HttpStatus.NOT_FOUND);
    }

    return plainToClass(CommentDTO, queryComments, {
      excludeExtraneousValues: true,
    });
  }

  public async createComment(
    comment: CreateCommentDTO,
    post: number,
    user: User,
  ): Promise<CommentDTO> {
    const createComment: Comments = this.commentsRepository.create(comment);

    const linkParentPost: Posts = await this.postsRepository.findOne({
      id: post,
    });
    const linkCreator: User = await this.userRepository.findOne({
      username: user.username,
    });

    createComment.user = Promise.resolve(linkCreator);
    createComment.post = Promise.resolve(linkParentPost);
    createComment.userId = linkCreator.id;
    createComment.username = linkCreator.username;

    const saveComment: Comments = await this.commentsRepository.save(
      createComment,
    );

    return plainToClass(CommentDTO, saveComment, {
      excludeExtraneousValues: true,
    });
  }

  public async updateCommentInPost(
    comment: UpdateCommentDTO,
  ): Promise<CommentDTO> {
    const oldComment: Comments = await this.findCommentById(comment.id);
    oldComment.title = comment.title;
    oldComment.content = comment.content;

    const updatedComment: Comments = await this.commentsRepository.save(
      oldComment,
    );

    return plainToClass(CommentDTO, updatedComment, {
      excludeExtraneousValues: true,
    });
  }

  public async deleteCommentInPost(id: number): Promise<CommentDTO> {
    const foundComment: Comments = await this.findCommentById(id);

    const deleteComment: Comments = await this.commentsRepository.save({
      ...foundComment,
      isDeleted: true,
    });

    return plainToClass(CommentDTO, deleteComment, {
      excludeExtraneousValues: true,
    });
  }

  public async findCommentById(id: number): Promise<Comments> {
    const commentToFind: Comments = await this.commentsRepository.findOne(id, {
      where: { isDeleted: false },
    });

    if (!commentToFind) {
      throw new NotFoundException(`Comment with id ${id} not found`);
    }

    // return plainToClass(CommentDTO, commentToFind, {
    //   excludeExtraneousValues: true,
    // });
    return commentToFind;
  }

  public async likeComment(id: number, user): Promise<Vote> {
    let votes: Vote[];
    let result: Vote;
    let foundVote: Vote;
    const foundComment: Comments = await this.findCommentById(id);

    if (foundComment === undefined || foundComment.isDeleted) {
      throw new ForumSystemError('No such post found', 404);
    }

    const checkIfUserVoted: Vote[] = foundComment.votes.filter(
      el => el.username === user.username,
    );

    if (checkIfUserVoted.length !== 0 && checkIfUserVoted[0].liked) {
      foundVote = await this.votesRepository.findOne({
        id: checkIfUserVoted[0].id,
      });

      result = await this.votesRepository.save({ ...foundVote, liked: false });
    }

    if (checkIfUserVoted.length !== 0 && !checkIfUserVoted[0].liked) {
      foundVote = await this.votesRepository.findOne({
        id: checkIfUserVoted[0].id,
      });

      result = await this.votesRepository.save({ ...foundVote, liked: true });
    }

    if (checkIfUserVoted.length === 0) {
      const voteEntity: Vote = this.votesRepository.create();
      voteEntity.username = user.username;
      voteEntity.liked = true;

      votes = [...foundComment.votes, voteEntity];

      await this.votesRepository.save(voteEntity);

      await this.commentsRepository.save({
        ...foundComment,
        votes,
      });

      return voteEntity;
    }

    return result;
  }

  public async commentVotes(id: number): Promise<Vote[]> {
    const foundComment: Comments = await this.commentsRepository.findOne({
      relations: ['votes'],
      where: { id },
    });

    if (foundComment === undefined || foundComment.isDeleted) {
      throw new ForumSystemError('No such comment found', 404);
    }

    return foundComment.votes;
  }
}
