import { Expose, Transform } from 'class-transformer';
import { IsString, IsDate, IsNumber } from 'class-validator';
import { User } from './../../database/entities/user.entity';
import { Posts } from './../../database/entities/posts.entity';

export class CommentDTO {
  @Expose()
  public id: number;

  @Expose()
  public title: string;

  @Expose()
  public content: string;

  @Expose()
  public username: string;

  @Expose()
  public userId: string;

  // @Expose()
  // post: Posts;

  @Expose()
  @Transform((_, vote) => {
    if(vote.votes) {
      return vote.votes;
    } else {
      return false;
    }
  })
  public votes: boolean;

  @Expose()
  public createdOn: Date;

  @Expose()
  public lastUpdatedOn: Date;
}
