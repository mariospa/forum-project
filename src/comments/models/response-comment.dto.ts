import { Expose } from 'class-transformer';

export class CommentResponseMessageDTO {
  @Expose()
  public msg: string;
}
