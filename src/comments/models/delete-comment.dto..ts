import { IsNumber, IsBoolean } from 'class-validator';
import { Expose } from 'class-transformer';

export class DeleteCommentDTO {
  @Expose()
  @IsNumber()
  public id: number;

  @Expose()
  @IsBoolean()
  public isDeleted: boolean;
}
