import { IsString, Length } from 'class-validator';
import { Expose } from 'class-transformer';

export class CreateCommentDTO {
  @Expose()
  @IsString()
  @Length(2, 30)
  public title: string;

  @Expose()
  @IsString()
  @Length(2, 1000)
  public content: string;
}
