import { IsString, Length, IsNumber } from 'class-validator';
import { Expose } from 'class-transformer';

export class UpdateCommentDTO {
  @Expose()
  @IsNumber()
  public id: number;
  @Expose()
  @IsString()
  @Length(2, 1000)
  public content: string;
  @Expose()
  @IsString()
  @Length(2, 30)
  public title: string;
}
