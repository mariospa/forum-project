import { Test, TestingModule } from '@nestjs/testing';
import { CommentsController } from './comments.controller';
import { CommentsDataService } from './comments.data-service';
import { CommentDTO } from './models/comment.dto';
import { UpdateCommentDTO } from './models/update-comment.dto';
import { User } from '../database/entities/user.entity';
import { Vote } from '../database/entities/vote.entity';

describe('Comments Controller', () => {
  let commentsService;
  let controller: CommentsController;
  beforeEach(async () => {
    commentsService = {
      findAllCommentsForPost: jest.fn(),

      commentsCount: jest.fn(),

      createComment: jest.fn(),

      updateCommentInPost: jest.fn(),

      deleteCommentInPost: jest.fn(),

      queryComments: jest.fn(),

      likeComment: jest.fn(),

      commentVotes: jest.fn(),
    };
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CommentsController],
      providers: [
        {
          provide: CommentsDataService,
          useValue: commentsService,
        },
      ],
    }).compile();

    controller = module.get<CommentsController>(CommentsController);

    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAllCommentsForPost() should ', () => {
    it('call findAllCommentsForPost() once with correct input data', async () => {
      // Arrange
      const mockId = '1';
      const mockLimit = 10;
      const mockSkip = 0;
      const mockService = jest.spyOn(commentsService, 'findAllCommentsForPost');

      // Act
      await controller.findAllCommentsForPost(mockId);

      // Assert
      expect(mockService).toHaveBeenCalledTimes(1);
      expect(mockService).toHaveBeenCalledWith(+mockId, mockLimit, mockSkip);
    });

    it('return result of findAllCommentsForPost() execution', async () => {
      // Arrange
      const mockComment = new CommentDTO();

      jest
        .spyOn(commentsService, 'findAllCommentsForPost')
        .mockImplementation(() => Promise.resolve([mockComment]));

      // Act
      const result = await controller.findAllCommentsForPost('1');

      // Assert
      expect(result).toStrictEqual([mockComment]);
    });
  });

  describe('commentsCount() should', () => {
    it('call commentsCount() once with correct input data', async () => {
      // Arrange
      const mockId = '1';
      const mockCommentsCount = 10;
      const isDeleted = false;
      const spyCommentsCount = jest
        .spyOn(commentsService, 'commentsCount')
        .mockImplementation(() => Promise.resolve(mockCommentsCount));

      // Act
      await controller.commentsCount(mockId);

      // Assert
      expect(spyCommentsCount).toBeCalledTimes(1);
      expect(spyCommentsCount).toBeCalledWith(+mockId, isDeleted);
    });

    it('return result of commentsCount() execution', async () => {
      // Arrange
      const mockId = '1';
      const mockCommentsCount = 10;
      jest
        .spyOn(commentsService, 'commentsCount')
        .mockImplementation(() => Promise.resolve(mockCommentsCount));

      // Act
      const result = await controller.commentsCount(mockId);

      // Assert
      expect(result).toEqual(mockCommentsCount);
    });
  });

  describe('queryComments() should ', () => {
    it('call queryComments() once with correct input data', async () => {
      // Arrange
      const mockSearch = 'test';
      const mockComment = new CommentDTO();
      const spyQuery = jest
        .spyOn(commentsService, 'queryComments')
        .mockImplementation(() => Promise.resolve([mockComment]));

      // Act
      await controller.queryComments(mockSearch);

      // Assert
      expect(spyQuery).toHaveBeenCalledTimes(1);
      expect(spyQuery).toHaveBeenCalledWith(mockSearch);
    });

    it('return result of queryComments() execution', async () => {
      // Arrange
      const mockSearch = 'test';
      const mockComment = new CommentDTO();
      jest
        .spyOn(commentsService, 'queryComments')
        .mockImplementation(() => Promise.resolve([mockComment]));

      // Act
      const result = await controller.queryComments(mockSearch);

      // Assert
      expect(result).toEqual([mockComment]);
    });
  });

  describe('addNewCommentToPost() should', () => {
    it('call createComment() once with correct input data', async () => {
      // Arrange
      const body = { title: 'Title', content: 'Content' };
      const postId = '1';
      const user = {};
      const mockCommentDTO = new CommentDTO();
      const spyCreateComment = jest
        .spyOn(commentsService, 'createComment')
        .mockImplementation(() => Promise.resolve(mockCommentDTO));

      // Act
      await controller.addNewCommentToPost(body, postId, user);

      // Assert
      expect(spyCreateComment).toHaveBeenCalledTimes(1);
      expect(spyCreateComment).toHaveBeenCalledWith(body, postId, user);
    });

    it('return result of createComment() execution', async () => {
      // Arrange
      const body = { title: 'Title', content: 'Content' };
      const postId = '1';
      const user = {};
      const mockCommentDTO = new CommentDTO();
      jest
        .spyOn(commentsService, 'createComment')
        .mockImplementation(() => Promise.resolve(mockCommentDTO));

      // Act
      const result = await controller.addNewCommentToPost(body, postId, user);

      // Assert
      expect(result).toEqual(mockCommentDTO);
    });
  });

  describe('updateCommentInPost() should', () => {
    it('call updateCommentInPost()once with correct input data', async () => {
      // Arrange
      const comment: UpdateCommentDTO = {
        id: 1,
        title: 'Title',
        content: 'Updated comment',
      };
      const mockCommentDTO = new CommentDTO();

      const spyUpdaetComment = jest
        .spyOn(commentsService, 'updateCommentInPost')
        .mockImplementation(() => Promise.resolve(mockCommentDTO));

      // Act
      await controller.updateCommentInPost(comment);

      // Assert
      expect(spyUpdaetComment).toHaveBeenCalledTimes(1);
      expect(spyUpdaetComment).toHaveBeenCalledWith(comment);
    });

    it('return result of updateCommentInPost() execution', async () => {
      // Arrange
      const comment: UpdateCommentDTO = {
        id: 1,
        title: 'Title',
        content: 'Updated comment',
      };
      const mockCommentDTO = new CommentDTO();
      jest
        .spyOn(commentsService, 'updateCommentInPost')
        .mockImplementation(() => Promise.resolve(mockCommentDTO));

      // Act
      const result = await controller.updateCommentInPost(comment);

      // Assert
      expect(result).toEqual(mockCommentDTO);
    });
  });

  describe('deleteCommentInPost() should ', () => {
    it('call deleteCommentInPost() once with correct input data', async () => {
      // Arrange
      const mockId = '1';
      const mockCommentDTO = new CommentDTO();
      const spyDeleteComment = jest
        .spyOn(commentsService, 'deleteCommentInPost')
        .mockImplementation(() => Promise.resolve(mockCommentDTO));

      // Act
      await controller.deleteCommentInPost(mockId);

      // Assert
      expect(spyDeleteComment).toHaveBeenCalledTimes(1);
      expect(spyDeleteComment).toHaveBeenCalledWith(+mockId);
    });

    it('return result of deleteCommentInPost() execution', async () => {
      // Arrange
      const mockId = '1';
      const mockCommentDTO = new CommentDTO();
      jest
        .spyOn(commentsService, 'deleteCommentInPost')
        .mockImplementation(() => Promise.resolve(mockCommentDTO));

      // Act
      const result = await controller.deleteCommentInPost(mockId);

      // Assert
      expect(result).toEqual(mockCommentDTO);
    });
  });

  describe('likeComment() should', () => {
    it('call likeComment() once with correct input data', async () => {
      // Act
      const mockId = '1';
      const mockUser = new User();
      const mockVote = new Vote();
      const spyLikeComment = jest
        .spyOn(commentsService, 'likeComment')
        .mockImplementation(() => Promise.resolve(mockVote));
      // Arrange
      await controller.likeComment(mockId, mockUser);

      // Assert
      expect(spyLikeComment).toBeCalledTimes(1);
      expect(spyLikeComment).toBeCalledWith(+mockId, mockUser);
    });

    it('return result of likeComment() execution', async () => {
      // Act
      const mockId = '1';
      const mockUser = new User();
      const mockVote = new Vote();
      jest
        .spyOn(commentsService, 'likeComment')
        .mockImplementation(() => Promise.resolve(mockVote));
      // Arrange
      const result = await controller.likeComment(mockId, mockUser);

      // Assert
      expect(result).toEqual(mockVote);
    });
  });

  describe('commentVotes() should', () => {
    it('call commentVotes() once with correct input data', async () => {
      // Act
      const mockId = '1';
      const mockVote = new Vote();
      const spyCommentVotes = jest
        .spyOn(commentsService, 'commentVotes')
        .mockImplementation(() => Promise.resolve([mockVote]));
      // Arranges
      await controller.commentVotes(mockId);

      // Assert
      expect(spyCommentVotes).toBeCalledTimes(1);
      expect(spyCommentVotes).toBeCalledWith(+mockId);
    });

    it('return result of commentVotes() execution', async () => {
      // Act
      const mockId = '1';
      const mockVote = new Vote();
      const spyCommentVotes = jest
        .spyOn(commentsService, 'commentVotes')
        .mockImplementation(() => Promise.resolve([mockVote]));
      // Arranges
      await controller.commentVotes(mockId);

      // Assert
      expect(spyCommentVotes).toBeCalledTimes(1);
      expect(spyCommentVotes).toBeCalledWith(+mockId);
    });
  });
});
