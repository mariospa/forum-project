import { Comments } from './../database/entities/comments.entity';
import { User } from './../common/decorators/user.decorator';
import { CommentsDataService } from './comments.data-service';
import {
  Controller,
  Get,
  Post,
  HttpCode,
  HttpStatus,
  Body,
  Query,
  Put,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthGuardWithBlacklisting } from '../common/guards/auth-guard-blacklisting';
import { CommentDTO, CreateCommentDTO, UpdateCommentDTO } from './models';
import { plainToClass } from 'class-transformer';
import { Vote } from './../database/entities/vote.entity';

@Controller('posts/:id/comments')
// @UseGuards(AuthGuard('jwt'), AuthGuardWithBlacklisting)
export class CommentsController {
  public constructor(
    private readonly commentsDataService: CommentsDataService,
  ) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  public async findAllCommentsForPost(
    // @User() user,
    @Param('id') postId: string,
    @Query('limit') limit = 10,
    @Query('skip') skip = 0,
    // @Query('title') title?: string,
    // @Query('content') content?: string,
    // @Query('deleted') isDeleted = false,
  ): Promise<CommentDTO[]> {
    // if (user && !user.roles.map(el => el.name).includes('Admin')) {
    //   isDeleted = false;
    // }

    return await this.commentsDataService.findAllCommentsForPost(
      +postId,
      skip,
      limit,
    );
  }

  @Get('/count')
  @HttpCode(HttpStatus.OK)
  public async commentsCount(
    @Param('id') postId: string,
    @Query('deleted') isDeleted = false,
  ): Promise<number> {
    return await this.commentsDataService.commentsCount(+postId, isDeleted);
  }

  // @Get(':id')
  // @HttpCode(HttpStatus.OK)
  // public async findCommentById(id: string): Promise<CommentDTO> {
  //   const result: Comments = await this.commentsDataService.findCommentById(
  //     +id,
  //   );
  // }

  @Get()
  @HttpCode(HttpStatus.OK)
  public async queryComments(
    @Query('content') content: string,
    // @Query('author') author: string,
  ): Promise<CommentDTO[]> {
    const comments: CommentDTO[] = await this.commentsDataService.queryComments(
      content,
      // author,
    );

    return comments;
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard('jwt'), AuthGuardWithBlacklisting)
  public async addNewCommentToPost(
    @Body() body: CreateCommentDTO,
    @Param('id') postId,
    @User('user') user,
  ): Promise<CommentDTO> {
    return await this.commentsDataService.createComment(body, postId, user);
  }

  @Put()
  @HttpCode(HttpStatus.OK)
  public async updateCommentInPost(
    @Body() comment: UpdateCommentDTO,
  ): Promise<CommentDTO> {
    return await this.commentsDataService.updateCommentInPost(comment);
  }

  @Delete('/:id')
  @HttpCode(HttpStatus.OK)
  public async deleteCommentInPost(
    @Param('id') id: string,
  ): Promise<CommentDTO> {
    return await this.commentsDataService.deleteCommentInPost(+id);
  }

  @Put('/:cId/like')
  @UseGuards(AuthGuard('jwt'), AuthGuardWithBlacklisting)
  @HttpCode(HttpStatus.OK)
  public async likeComment(
    @Param('cId') id: string,
    @User() user,
  ): Promise<Vote> {
    return await this.commentsDataService.likeComment(+id, user);
  }

  @Get('/:cId/votes')
  @HttpCode(HttpStatus.OK)
  public async commentVotes(@Param('cId') id: string): Promise<Vote[]> {
    return await this.commentsDataService.commentVotes(+id);
  }
}
