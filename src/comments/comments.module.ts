import { TypeOrmModule } from '@nestjs/typeorm';
import { Comments } from '../database/entities/comments.entity';
import { Module } from '@nestjs/common';
import { CommentsController } from './comments.controller';
import { CommentsDataService } from './comments.data-service';
import { Posts } from '../database/entities/posts.entity';
import { User } from '../database/entities/user.entity';
import { Vote } from '../database/entities/vote.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Comments, Posts, User, Vote])],
  controllers: [CommentsController],
  providers: [CommentsDataService],
})
export class CommentsModule {}
