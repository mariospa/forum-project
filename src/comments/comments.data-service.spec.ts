import { User } from './../database/entities/user.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Test, TestingModule } from '@nestjs/testing';
import { Repository, Like } from 'typeorm';
import { Posts } from '../database/entities/posts.entity';
import { Comments } from '../database/entities/comments.entity';
import { CommentsDataService } from './../../src/comments/comments.data-service';
import { Vote } from '../database/entities/vote.entity';
import { HttpException, NotFoundException } from '@nestjs/common';
import { CreateCommentDTO } from './models/create-comment.dto';
import { UpdateCommentDTO } from './models/update-comment.dto';
import { ForumSystemError } from '../common/exceptions/forum-system.error';

describe('CommentsDataService', () => {
  let service: CommentsDataService;
  const commentRepo: Partial<Repository<Comments>> = {
    create: jest.fn(),

    find: jest.fn(),

    save: jest.fn(),

    findOne: jest.fn(),

    count: jest.fn(),
  };
  const postRepo: Partial<Repository<Posts>> = {
    findOne: jest.fn(),
  };
  const userRepo: Partial<Repository<User>> = {
    findOne: jest.fn(),
  };

  const votesRepo: Partial<Repository<Vote>> = {
    save: jest.fn(),

    findOne: jest.fn(),

    create: jest.fn(),
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CommentsDataService,
        {
          provide: getRepositoryToken(Comments),
          useValue: commentRepo,
        },
        {
          provide: getRepositoryToken(Posts),
          useValue: postRepo,
        },
        {
          provide: getRepositoryToken(User),
          useValue: userRepo,
        },
        {
          provide: getRepositoryToken(Vote),
          useValue: votesRepo,
        },
      ],
    }).compile();
    service = module.get<CommentsDataService>(CommentsDataService);
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  describe('findAllCommentsForPost() should', () => {
    it('call find() once with correct input data', async () => {
      // Arrange
      const mockID = 3;
      const mockTake = 10;
      const mockSkip = 0;
      const mockComment = new Comments();
      const mockORM = {
        where: {
          post: mockID,
          isDeleted: false,
        },
        skip: mockSkip,
        take: mockTake,
      };

      const spyCommentRepoFind = jest
        .spyOn(commentRepo, 'find')
        .mockResolvedValue([mockComment]);

      // Act
      await service.findAllCommentsForPost(mockID);

      // Assert
      expect(spyCommentRepoFind).toBeCalledTimes(1);
      expect(spyCommentRepoFind).toBeCalledWith(mockORM);
    });

    it('return appropriate message, if post has no comments yet', async () => {
      // Arrange
      const mockID = 3;
      jest.spyOn(commentRepo, 'find').mockResolvedValue([]);
      // Act
      const result = await service.findAllCommentsForPost(mockID);
      // Assert
      expect(result).toEqual({ message: 'This post has no comments yet!' });
    });

    it('return result of find() execution', async () => {
      // Arrange
      const mockID = 3;
      const mockComment = new Comments();
      const mockCommentDTO = {
        id: undefined,
        content: undefined,
        userId: undefined,
        username: undefined,
        createdOn: undefined,
        lastUpdatedOn: undefined,
        title: undefined,
        votes: false,
      };

      jest.spyOn(commentRepo, 'find').mockResolvedValue([mockComment]);
      // Act
      const result = await service.findAllCommentsForPost(mockID);

      // Assert
      expect(result).toEqual([mockCommentDTO]);
    });
  });

  describe('commentsCount() should', () => {
    it('call count() once with correct input data', async () => {
      // Arrange
      const mockId = 3;
      const mockCount = 4;
      const mockORM = {
        where: {
          post: mockId,
          isDeleted: false,
        },
      };
      const spyCount = jest
        .spyOn(commentRepo, 'count')
        .mockResolvedValue(mockCount);
      // Act
      await service.commentsCount(mockId);

      // Assert
      expect(spyCount).toBeCalledTimes(1);
      expect(spyCount).toBeCalledWith(mockORM);
    });

    it('return result of count() execution', async () => {
      // Arrange
      const mockId = 3;
      const mockCount = 4;
      jest.spyOn(commentRepo, 'count').mockResolvedValue(mockCount);
      // Act
      const result = await service.commentsCount(mockId);
      // Assert
      expect(result).toEqual(mockCount);
    });
  });

  describe('queryCommets() should', () => {
    it(`call find() once with correct input data`, async () => {
      // Arrange
      const mockSearch = 'TEST';
      const mockComment = new Comments();
      const mockORM = {
        where: {
          title: Like(`%${mockSearch.toLowerCase()}`),
          content: Like(`%${mockSearch.toLowerCase()}`),
          isDeleted: false,
        },
      };
      const spyFind = jest
        .spyOn(commentRepo, 'find')
        .mockResolvedValue([mockComment]);
      // Act
      await service.queryComments(mockSearch);

      // Assert
      expect(spyFind).toBeCalledTimes(1);
      expect(spyFind).toBeCalledWith(mockORM);
    });

    it(`throw NotFound if find() returns a falsy value `, async () => {
      // Arrange
      const mockSearch = 'TEST';
      jest
        .spyOn(commentRepo, 'find')
        .mockImplementation(() => Promise.resolve(undefined));
      // Act & Assert
      expect(service.queryComments(mockSearch)).rejects.toThrowError(
        HttpException,
      );
    });

    it('return result of find() execution', async () => {
      // Act
      const mockSearch = 'TEST';
      const mockComment = new Comments();
      const mockCommentDTO = {
        id: undefined,
        content: undefined,
        userId: undefined,
        username: undefined,
        createdOn: undefined,
        lastUpdatedOn: undefined,
        title: undefined,
        votes: false,
      };
      jest
        .spyOn(commentRepo, 'find')
        .mockImplementation(() => Promise.resolve([mockComment]));
      // Act
      const result = await service.queryComments(mockSearch);
      // Assert
      expect(result).toEqual([mockCommentDTO]);
    });
  });

  describe('createComment() should', () => {
    it('call commentstRepository create() once with correct input data', async () => {
      // Arrange
      const mockCreateCommentDTO = new CreateCommentDTO();
      const mockPostId = 1;
      const mockComment = new Comments();
      const mockUser = new User();
      mockUser.id = 1;
      mockUser.username = 'mock username';
      const mockPost = new Posts();
      mockPost.id = mockPostId;

      const spyCommentsRepoCreate = jest
        .spyOn(commentRepo, 'create')
        .mockReturnValue(mockComment);
      jest.spyOn(postRepo, 'findOne').mockResolvedValue(mockPost);
      jest.spyOn(userRepo, 'findOne').mockResolvedValue(mockUser);
      jest.spyOn(commentRepo, 'save').mockResolvedValue(mockComment);

      // Act
      await service.createComment(mockCreateCommentDTO, mockPostId, mockUser);

      // Assert
      expect(spyCommentsRepoCreate).toBeCalledTimes(1);
      expect(spyCommentsRepoCreate).toBeCalledWith(mockCreateCommentDTO);
    });

    it('call postsRepository findOne() once with correct input data', async () => {
      // Arrange
      const mockCreateCommentDTO = new CreateCommentDTO();
      const mockPostId = 1;
      const mockComment = new Comments();
      const mockUser = new User();
      mockUser.id = 1;
      mockUser.username = 'mock username';
      const mockPost = new Posts();
      mockPost.id = mockPostId;

      jest.spyOn(commentRepo, 'create').mockReturnValue(mockComment);
      const spyPostsRepoFindOne = jest
        .spyOn(postRepo, 'findOne')
        .mockResolvedValue(mockPost);
      jest.spyOn(userRepo, 'findOne').mockResolvedValue(mockUser);
      jest.spyOn(commentRepo, 'save').mockResolvedValue(mockComment);

      // Act
      await service.createComment(mockCreateCommentDTO, mockPostId, mockUser);

      // Assert
      expect(spyPostsRepoFindOne).toBeCalledTimes(1);
      expect(spyPostsRepoFindOne).toBeCalledWith({ id: mockPostId });
    });

    it('call postsRepository findOne() once with correct input data', async () => {
      // Arrange
      const mockCreateCommentDTO = new CreateCommentDTO();
      const mockPostId = 1;
      const mockComment = new Comments();
      const mockUser = new User();
      mockUser.id = 1;
      mockUser.username = 'mock username';
      const mockPost = new Posts();
      mockPost.id = mockPostId;
      const savedComment = { ...mockComment };
      savedComment.userId = mockUser.id;
      savedComment.username = mockUser.username;
      savedComment.user = Promise.resolve(mockUser);
      savedComment.post = Promise.resolve(mockPost);

      jest.spyOn(commentRepo, 'create').mockReturnValue(mockComment);
      jest.spyOn(postRepo, 'findOne').mockResolvedValue(mockPost);
      jest.spyOn(userRepo, 'findOne').mockResolvedValue(mockUser);
      const spyCommentsRepoSave = jest
        .spyOn(commentRepo, 'save')
        .mockResolvedValue(mockComment);

      // Act
      await service.createComment(mockCreateCommentDTO, mockPostId, mockUser);

      // Assert
      expect(spyCommentsRepoSave).toBeCalledTimes(1);
      expect(spyCommentsRepoSave).toBeCalledWith(savedComment);
    });

    it('return result of commentsRepository save() in correct DTO format', async () => {
      // Arrange
      const mockCreateCommentDTO = new CreateCommentDTO();
      const mockPostId = 1;
      const mockComment = new Comments();
      const mockUser = new User();
      mockUser.id = 1;
      mockUser.username = 'mock username';
      const mockPost = new Posts();
      mockPost.id = mockPostId;
      const savedComment = { ...mockComment };
      savedComment.userId = mockUser.id;
      savedComment.username = mockUser.username;
      savedComment.user = Promise.resolve(mockUser);
      savedComment.post = Promise.resolve(mockPost);

      jest.spyOn(commentRepo, 'create').mockReturnValue(mockComment);
      jest.spyOn(postRepo, 'findOne').mockResolvedValue(mockPost);
      jest.spyOn(userRepo, 'findOne').mockResolvedValue(mockUser);
      jest.spyOn(commentRepo, 'save').mockResolvedValue(mockComment);

      // Act
      const result = await service.createComment(
        mockCreateCommentDTO,
        mockPostId,
        mockUser,
      );

      // Assert
      expect(result).not.toHaveProperty('isDeleted');
    });
  });

  describe('updateCommentInPost() should', () => {
    it('call findCommentById() once with correct input data', async () => {
      // Arrange
      const mockUpdateCommentDTO = new UpdateCommentDTO();
      mockUpdateCommentDTO.id = 2;
      mockUpdateCommentDTO.title = 'Title';
      mockUpdateCommentDTO.content = 'Content';

      const mockComment = new Comments();
      mockComment.id = 2;
      mockComment.isDeleted = false;
      const spyFindCommentById = jest
        .spyOn(service, 'findCommentById')
        .mockResolvedValue(mockComment);
      jest.spyOn(commentRepo, 'save').mockResolvedValue(mockComment);

      // Act
      await service.updateCommentInPost(mockUpdateCommentDTO);
      // Assert
      expect(spyFindCommentById).toBeCalledTimes(1);
      expect(spyFindCommentById).toBeCalledWith(mockUpdateCommentDTO.id);
    });

    it('call commentsRepository save() once with correct input data', async () => {
      // Arrange
      const mockUpdateCommentDTO = new UpdateCommentDTO();
      mockUpdateCommentDTO.id = 2;
      mockUpdateCommentDTO.title = 'Title';
      mockUpdateCommentDTO.content = 'Content';

      const mockComment = new Comments();
      mockComment.id = 2;
      mockComment.isDeleted = false;
      jest.spyOn(service, 'findCommentById').mockResolvedValue(mockComment);
      const spyCommentsRepoSave = jest
        .spyOn(commentRepo, 'save')
        .mockResolvedValue(mockComment);

      // Act
      await service.updateCommentInPost(mockUpdateCommentDTO);
      // Assert
      expect(spyCommentsRepoSave).toBeCalledTimes(1);
      expect(spyCommentsRepoSave).toBeCalledWith({
        title: mockUpdateCommentDTO.title,
        content: mockUpdateCommentDTO.content,
        ...mockComment,
      });
    });
    it('return result of commentsRepository save execution in correct DTO format', async () => {
      // Arrange
      const mockUpdateCommentDTO = new UpdateCommentDTO();
      mockUpdateCommentDTO.id = 2;
      mockUpdateCommentDTO.title = 'Title';
      mockUpdateCommentDTO.content = 'Content';

      const mockComment = new Comments();
      mockComment.id = 2;
      mockComment.isDeleted = false;
      jest.spyOn(service, 'findCommentById').mockResolvedValue(mockComment);
      jest.spyOn(commentRepo, 'save').mockResolvedValue(mockComment);

      // Act
      const result = await service.updateCommentInPost(mockUpdateCommentDTO);
      // Assert
      expect(result).not.toHaveProperty('isDeleted');
    });
  });
  describe('deleteCommentInPost() should', () => {
    it('call findCommentById() once with correct input data', async () => {
      // Arrange
      const mockId = 1;
      const mockComment = new Comments();
      mockComment.id = mockId;
      mockComment.isDeleted = true;
      const spyFindCommentById = jest
        .spyOn(service, 'findCommentById')
        .mockResolvedValue(mockComment);
      jest.spyOn(commentRepo, 'save').mockResolvedValue(mockComment);
      // Act
      await service.deleteCommentInPost(mockId);
      // Assert
      expect(spyFindCommentById).toBeCalledTimes(1);
      expect(spyFindCommentById).toBeCalledWith(mockId);
    });

    it('call commentsRepository save() once with correct input data', async () => {
      // Arrange
      const mockId = 1;
      const mockComment = new Comments();
      mockComment.id = mockId;
      jest.spyOn(service, 'findCommentById').mockResolvedValue(mockComment);
      const spyCommentsRepoSave = jest
        .spyOn(commentRepo, 'save')
        .mockResolvedValue(mockComment);
      // Act
      await service.deleteCommentInPost(mockId);
      // Assert
      expect(spyCommentsRepoSave).toBeCalledTimes(1);
      expect(spyCommentsRepoSave).toBeCalledWith({
        ...mockComment,
        isDeleted: true,
      });
    });

    it('return result of commentsRepository save execution in correct DTO format', async () => {
      // Arrange
      const mockId = 1;
      const mockComment = new Comments();
      mockComment.id = mockId;
      jest.spyOn(service, 'findCommentById').mockResolvedValue(mockComment);
      jest.spyOn(commentRepo, 'save').mockResolvedValue(mockComment);
      // Act
      const result = await service.deleteCommentInPost(mockId);
      // Assert
      expect(result).not.toHaveProperty('isDeleted');
    });
  });

  describe('findCommentById() should ', () => {
    it('call commentsReposiroty findOne() once with correct input data', async () => {
      // Arrange
      const mockId = 1;
      const mockComment = new Comments();
      mockComment.id = mockId;
      mockComment.isDeleted = false;
      const spyCommentsRepoFindOne = jest
        .spyOn(commentRepo, 'findOne')
        .mockResolvedValue(mockComment);
      // Act
      await service.findCommentById(mockId);

      // Assert
      expect(spyCommentsRepoFindOne).toBeCalledTimes(1);
      expect(spyCommentsRepoFindOne).toBeCalledWith(mockId, {
        where: { isDeleted: false },
      });
    });
    it('throw NotFoundException if comment is not found', async () => {
      // Arrange
      const mockId = 1;
      const mockComment = new Comments();
      mockComment.id = mockId;
      mockComment.isDeleted = false;
      jest.spyOn(commentRepo, 'findOne').mockResolvedValue(undefined);

      // Act & Assert
      expect(service.findCommentById(mockId)).rejects.toThrowError(
        NotFoundException,
      );
    });
    it('return result of commentsRepository findOne() execution', async () => {
      // Arrange
      const mockId = 1;
      const mockComment = new Comments();
      mockComment.id = mockId;
      mockComment.isDeleted = false;
      jest.spyOn(commentRepo, 'findOne').mockResolvedValue(mockComment);

      // Act
      const result = await service.findCommentById(mockId);

      // Assert
      expect(result).toEqual(mockComment);
    });
  });

  describe('likeComment() should', () => {
    it('', async () => {
      // Arrange
      // Act
      // Assert
    });
  });

  describe('commentVotes() should', () => {
    it('call commentsRepository findOne() once with correct input data', async () => {
      // Arrange
      const mockId = 1;
      const mockComment = new Comments();
      mockComment.id = mockId;
      mockComment.votes = [];
      const spyCommentsRepoFindOne = jest
        .spyOn(commentRepo, 'findOne')
        .mockResolvedValue(mockComment);

      // Act
      await service.commentVotes(mockId);

      // Assert
      expect(spyCommentsRepoFindOne).toHaveBeenCalledTimes(1);
      expect(spyCommentsRepoFindOne).toHaveBeenCalledWith({
        relations: ['votes'],
        where: { id: mockId },
      });
    });

    it('throw ForumSystemError if comment is undefined', async () => {
      // Arrange
      const mockId = 1;
      const mockComment = new Comments();
      const mockVote = new Vote();
      mockComment.id = mockId;
      mockComment.votes = [mockVote];
      jest.spyOn(commentRepo, 'findOne').mockResolvedValue(undefined);

      // Act & Assert
      expect(service.commentVotes(mockId)).rejects.toThrowError(
        ForumSystemError,
      );
    });

    it('return the votes for given comment', async () => {
      // Arrange
      const mockId = 1;
      const mockComment = new Comments();
      const mockVote = new Vote();
      mockComment.id = mockId;
      mockComment.votes = [mockVote];
      jest.spyOn(commentRepo, 'findOne').mockResolvedValue(mockComment);

      // Act
      const result = await service.commentVotes(mockId);

      // Assert
      expect(result).toEqual(mockComment.votes);
    });
  });
});
