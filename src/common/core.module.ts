import { Module, Global } from '@nestjs/common';
import { AuthModule } from '../auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import * as Joi from '@hapi/joi';

@Global()
@Module({
  imports: [
    AuthModule,
  ],
  exports: [AuthModule],
})
export class CoreModule {}