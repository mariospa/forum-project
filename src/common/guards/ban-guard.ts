import { ForumSystemError } from './../exceptions/forum-system.error';
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import * as moment from 'moment';

@Injectable()
export class BanGuard implements CanActivate {
  public canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    const user = request.user;

    if(user.bans == null) {
      return true;
    }
    if(!user.bans.isBanned) {
      return true;
    }
    if(user.bans.isBanned){
        throw new ForumSystemError('You have been banned!', 404);
    }
  }
}
