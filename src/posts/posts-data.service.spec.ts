import { getRepositoryToken } from '@nestjs/typeorm';
import { Test, TestingModule } from '@nestjs/testing';
import { PostsDataService } from './posts-data.service';
import { Repository } from 'typeorm';
import { Posts } from './../database/entities/posts.entity';
import { Comments } from '../database/entities/comments.entity';
import { User } from '../database/entities/user.entity';
import { Vote } from '../database/entities/vote.entity';

describe('CommentsDataService', () => {
  let service: PostsDataService;
  const postRepo: Partial<Repository<Posts>> = {
    create() {
      return null;
    },
    find() {
      return null;
    },
    save() {
      return null;
    },
    findOne() {
      return null;
    },
  };
  const commentRepo: Partial<Repository<Comments>> = {
    findOne() {
      return null;
    },
  };
  const userRepo: Partial<Repository<User>> = {};
  const voteRepo: Partial<Repository<Vote>> = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PostsDataService,
        {
          provide: getRepositoryToken(Comments),
          useValue: commentRepo,
        },
        {
          provide: getRepositoryToken(Posts),
          useValue: postRepo,
        },
        {
          provide: getRepositoryToken(User),
          useValue: userRepo,
        },
        {
          provide: getRepositoryToken(Vote),
          useValue: voteRepo,
        },
      ],
    }).compile();
    service = module.get<PostsDataService>(PostsDataService);
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('allPosts should ', async () => {
    // Arrange
    // Act
    // Assert
  });

  it(`createPost should `, async () => {
    // Arrange
    // Act
    // Assert
  });

  it('updatePost should ', async () => {
    // Arrange
    // Act
    // Assert
  });

  it('findPostById should ', async () => {
    // Arrange
    // Act
    // Assert
  });

  it('deletePost should ', async () => {
    // Arrange
    // Act
    // Assert
  });

  it('likePost should ', async () => {
    // Arrange
    // Act
    // Assert
  });

  it('postVotes should ', async () => {
    // Arrange
    // Act
    // Assert
  });
});
