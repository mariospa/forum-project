import { IsString, Length } from 'class-validator';

export class CreatePostDTO {
    @Length(2, 20)
    @IsString()
    public title: string;
   
    @Length(2, 200000)
    @IsString()
    public content: string;
}
