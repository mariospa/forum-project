import { Vote } from './../../database/entities/vote.entity';
import { Expose, Transform } from 'class-transformer';
import { User } from './../../database/entities/user.entity';

export class PostDTO {
  @Expose()
  public id: number;

  @Expose()
  public title: string;

  @Expose()
  // @Transform((_, obj) => (obj as any).__user__.id)
  public userId: number;

  @Expose()
  public username: string;

  @Expose()
  public content: string;

  @Expose()
  public createdOn: Date;

  @Expose()
  public updatedOn: Date;

  @Expose()
  @Transform((_, vote) => {
    if(vote.votes) {
      return vote.votes;
    } else {
      return false;
    }
  })
  public votes: boolean;
}
