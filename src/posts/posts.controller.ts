import { UserRole } from './../users/enums/user-role.enum';

import { ShowUserDTO } from './../users/models/show-user.dto';
import { CreatePostDTO } from './models/create-post.dto';
import { PostDTO } from './models/post.dto';
import { plainToClass } from 'class-transformer';
import { AuthGuardWithBlacklisting } from '../common/guards/auth-guard-blacklisting';
import { PostsDataService } from './posts-data.service';
import {
  Controller,
  Get,
  Post,
  HttpCode,
  HttpStatus,
  Body,
  Query,
  Put,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { User } from 'src/common/decorators/user.decorator';
import { userInfo } from 'os';
import { Posts } from 'src/database/entities/posts.entity';
import { Vote } from 'src/database/entities/vote.entity';

@Controller()
export class PostsController {
  public constructor(private readonly postsDataService: PostsDataService) { }

  @Get('posts')
  @HttpCode(HttpStatus.OK)
  public async allPosts(
    @User() user,
    @Query('limit') limit?: string,
    @Query('skip') skip?: string,
    @Query('title') title?: string,
    @Query('content') content?: string,
    @Query('deleted') isDeleted = false,
  ): Promise<PostDTO[]> {
    if (user && !user.roles.map(el => el.name).includes('Admin')) {
      isDeleted = false;
    }
    return await this.postsDataService.allPosts(
      +limit,
      +skip,
      title,
      content,
      isDeleted,
    );
  }

  @Get('posts/count')
  @HttpCode(HttpStatus.OK)
  public async postCount(
    @Query('title') title?: string,
    @Query('author') author?: string,
    @Query('content') content?: string,
    @Query('deleted') isDeleted = false,
  ) {
    return await this.postsDataService.postCount(
      title,
      author,
      content,
      isDeleted,
    );
  }

  @Post('post')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard('jwt'), AuthGuardWithBlacklisting)
  public async createPost(
    @Body() body: CreatePostDTO,
    @User() user: ShowUserDTO,
  ): Promise<PostDTO> {
    const savedPost: PostDTO = await this.postsDataService.createPost(
      body,
      user,
    );

    return savedPost;
  }

  @Get('posts/:id')
  @HttpCode(HttpStatus.OK)
  public async getPost(@Param('id') id: string): Promise<PostDTO> {
    const post: Posts = await this.postsDataService.findPostById(+id);
    return plainToClass(PostDTO, post, {
      excludeExtraneousValues: true,
    });
  }

  @Put('posts/:id')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async updatePost(
    @User() user,
    @Param('id') id: string,
    @Body() body,
  ): Promise<PostDTO> {
    return await this.postsDataService.updatePost(
      +id,
      body,
      user,
    );
  }

  @Delete('posts/:id')
  @HttpCode(HttpStatus.OK)
  public async deletePost(@Param('id') id: string): Promise<PostDTO> {
    return await this.postsDataService.deletePost(+id);
  }

  @Put('posts/:id/like')
  @UseGuards(AuthGuard('jwt'), AuthGuardWithBlacklisting)
  @HttpCode(HttpStatus.OK)
  public async likePost(@Param('id') id: string, @User() user): Promise<Vote> {
    return await this.postsDataService.likePost(+id, user);
  }

  @Get('posts/:id/votes')
  @HttpCode(HttpStatus.OK)
  public async postVotes(@Param('id') id: string): Promise<Vote[]> {
    return await this.postsDataService.postVotes(+id);
  }
}
