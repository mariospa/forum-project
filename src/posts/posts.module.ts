import { Vote } from './../database/entities/vote.entity';
import { User } from './../database/entities/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Posts } from './../database/entities/posts.entity';
import { Module } from '@nestjs/common';
import { PostsController } from './posts.controller';
import { PostsDataService } from './posts-data.service';

@Module({
  imports: [TypeOrmModule.forFeature([Posts, User, Vote])],
  controllers: [PostsController],
  providers: [PostsDataService]
})
export class PostsModule {}
