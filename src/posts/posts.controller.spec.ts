import { Test, TestingModule } from '@nestjs/testing';
import { PostsController } from './posts.controller';
import { PostsDataService } from './posts-data.service';
import { User } from 'src/database/entities/user.entity';

describe('Posts Controller', () => {
  let controller: PostsController;
  const PostsService: Partial<PostsDataService> = {
    findPostById() {
      return null;
    },
    allPosts() {
      return null;
    },
    createPost() {
      return null;
    },
    updatePost() {
      return null;
    },
    deletePost() {
      return null;
    },
    likePost() {
      return null;
    },
    postVotes() {
      return null;
    },
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PostsController], // token
      providers: [
        {
          provide: PostsDataService,
          useValue: PostsService,
        },
      ],
    }).compile();

    controller = module.get<PostsController>(PostsController);

    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('allPosts should call allPosts', async () => {
    // Arrange
    const user: any = {};
    jest.spyOn(PostsService, 'allPosts');

    // Act
    await controller.allPosts(user);

    // Assert
    expect(PostsService.allPosts).toHaveBeenCalledTimes(1);
  });

  it('createPost should ', async () => {
    // Arrange
    // Act
    // Assert
  });

  it('getPost should ', async () => {
    // Arrange
    // Act
    // Assert
  });

  it('updatePost should ', async () => {
    // Arrange
    // Act
    // Assert
  });

  it('deletePost should ', async () => {
    // Arrange
    // Act
    // Assert
  });

  it('likePost should ', async () => {
    // Arrange
    // Act
    // Assert
  });

  it('postVotes should ', async () => {
    // Arrange
    // Act
    // Assert
  });
});
