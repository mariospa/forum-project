import { PostsModule } from './posts.module';
import { Vote } from './../database/entities/vote.entity';
import { UpdatePostDTO } from './models/update-post.dto';
import { ShowUserDTO } from './../users/models/show-user.dto';
import { ForumSystemError } from '../common/exceptions/forum-system.error';
import { plainToClass } from 'class-transformer';
import { User } from './../database/entities/user.entity';
import { CreatePostDTO } from './models/create-post.dto';
import { PostDTO } from './models/post.dto';
import { Posts } from './../database/entities/posts.entity';
import { Injectable, Post } from '@nestjs/common';
import { Repository, In, Like } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class PostsDataService {
  public constructor(
    @InjectRepository(Posts)
    private readonly postsRepository: Repository<Posts>,
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
    @InjectRepository(Vote)
    private readonly votesRepository: Repository<Vote>,
  ) { }

  public async allPosts(
    limit?: number,
    skip?: number,
    title?: string,
    content?: string,
    deleted = false,
  ): Promise<PostDTO[]> {

    const options = {
      where: {
        isDeleted: deleted,
        ...(title && { title: Like(`%${title}%`) }),
        ...(content && { content: Like(`%${content}%`) })
      },
      ...(skip && { skip: skip }),
      ...(limit && { take: limit }),

    }

    const result: Posts[] = await this.postsRepository.find(options);

    return plainToClass(PostDTO, result, {
      excludeExtraneousValues: true,
    });
  }

  public async postCount(
    title?: string,
    author?: string,
    content?: string,
    deleted = false,
  ): Promise<number> {

    const options = {
      where: {
        isDeleted: deleted,
        ...(title && { title: Like(`%${title}%`) }),
        ...(content && { content: Like(`%${content}%`) })
      }
    }
    console.log(options)
    return await this.postsRepository.count(options);
  }

  public async createPost(
    post: CreatePostDTO,
    user: ShowUserDTO,
  ): Promise<PostDTO> {
    const postEntity: Posts = this.postsRepository.create(post);
    const foundUser: User = await this.usersRepository.findOne({
      username: user.username,
    });

    if (foundUser === undefined || foundUser.isDeleted) {
      throw new ForumSystemError('No such user found', 404);
    }

    postEntity.user = Promise.resolve(foundUser);
    postEntity.userId = foundUser.id;
    postEntity.username = foundUser.username;
    const savedPost: Posts = await this.postsRepository.save(postEntity);

    return plainToClass(PostDTO, savedPost, {
      excludeExtraneousValues: true,
    });
  }

  public async updatePost(
    id: number,
    post: UpdatePostDTO,
    user: ShowUserDTO,
  ): Promise<PostDTO> {
    const oldPost: Posts = await this.findPostById(id);

    const oldPostCreatedBy: User = await this.usersRepository.findOne({
      where: { user },
    });

    // if (oldPostCreatedBy.id !== user.id) {
    //   throw new ForumSystemError(
    //     'You dont have permission to edit this post!',
    //     666,
    //   );
    // }

    const updatedPost: Posts = { ...oldPost, ...post };
    const savedPost: Posts = await this.postsRepository.save(updatedPost);

    return plainToClass(PostDTO, savedPost, {
      excludeExtraneousValues: true,
    });
  }

  public async findPostById(id: number): Promise<Posts> {
    const foundPost: Posts = await this.postsRepository.findOne({ id });
    if (foundPost === undefined || foundPost.isDeleted) {
      throw new ForumSystemError('No such post found', 404);
    }
    return foundPost;
  }

  public async deletePost(id: number): Promise<PostDTO> {
    const foundPost: Posts = await this.findPostById(id);

    if (foundPost === undefined || foundPost.isDeleted) {
      throw new ForumSystemError('No such post found', 404);
    }
    foundPost.isDeleted = true;

    const deletedPost: Posts = await this.postsRepository.save(foundPost);

    return plainToClass(PostDTO, deletedPost, {
      excludeExtraneousValues: true,
    });
  }

  public async likePost(id: number, user): Promise<Vote> {
    let votes: Vote[];
    let result: Vote;
    let foundVote: Vote;
    const foundPost: Posts = await this.findPostById(id);

    if (foundPost === undefined || foundPost.isDeleted) {
      throw new ForumSystemError('No such post found', 404);
    }

    const checkIfUserVoted: Vote[] = foundPost.votes.filter(
      el => el.username === user.username,
    );

    if (checkIfUserVoted.length !== 0 && checkIfUserVoted[0].liked) {
      foundVote = await this.votesRepository.findOne({
        id: checkIfUserVoted[0].id,
      });

      result = await this.votesRepository.save({ ...foundVote, liked: false });
    }

    if (checkIfUserVoted.length !== 0 && !checkIfUserVoted[0].liked) {
      foundVote = await this.votesRepository.findOne({
        id: checkIfUserVoted[0].id,
      });

      result = await this.votesRepository.save({ ...foundVote, liked: true });
    }

    if (checkIfUserVoted.length === 0) {
      const voteEntity: Vote = this.votesRepository.create();
      voteEntity.username = user.username;
      voteEntity.liked = true;

      votes = [...foundPost.votes, voteEntity];

      await this.votesRepository.save(voteEntity);

      await this.postsRepository.save({
        ...foundPost,
        votes,
      });

      // console.log(saved);

      return voteEntity;
    }

    return result;
  }

  public async postVotes(id: number): Promise<Vote[]> {
    const foundPost: Posts = await this.postsRepository.findOne({
      relations: ['votes'],
      where: { id },
    });

    if (foundPost === undefined || foundPost.isDeleted) {
      throw new ForumSystemError('No such post found', 404);
    }

    return foundPost.votes;
  }
}
