import { Vote } from './vote.entity';
import { User } from './user.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Posts } from './posts.entity';

@Entity('comments')
export class Comments {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ nullable: false, length: 50 })
  public title: string;

  @Column({ nullable: false, type: 'text' })
  public content: string;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  public createdOn: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public lastUpdatedOn: Date;

  @Column({ nullable: false, default: false })
  public isDeleted: boolean;

  @Column({ nullable: false })
  public userId: number;

  @Column({ nullable: false })
  public username: string;

  @ManyToOne(type => User)
  public user: Promise<User>;

  @ManyToOne(type => Posts)
  public post: Promise<Posts>;

  @ManyToMany(type => Vote, { eager: true })
  @JoinTable()
  public votes: Vote[];
}
