import { Comments } from './comments.entity';
import { Posts } from './posts.entity';
import { Ban } from './ban.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
  OneToMany,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { Role } from './role.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ nullable: false, unique: true, length: 50 })
  public username: string;

  @Column({ nullable: false })
  public password: string;

  @Column({ nullable: true})
  public avatarUrl: string;

  @Column({ nullable: false, default: false })
  public isDeleted: boolean;

  @OneToMany(
    type => Posts,
    post => post.user,
    { lazy: true },
  )
  public posts: Promise<Posts[]>;

  @OneToMany(
    type => Comments,
    comment => comment.user,
    { lazy: true },
  )
  public comments: Promise<Comments[]>;

  @ManyToMany(type => Role, { eager: true })
  @JoinTable()
  public roles: Role[];

  @ManyToMany(type => User)
  @JoinTable()
  public friends: User[];

  @OneToOne(
    type => Ban,
    ban => ban.users,
)
  public bans: Ban;
}
