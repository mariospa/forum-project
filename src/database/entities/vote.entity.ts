import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('votes')
export class Vote {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ nullable: false })
  public username: string;

  @Column({ nullable: false })
  public liked: boolean;
}