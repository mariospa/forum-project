import { Vote } from './vote.entity';
import { User } from './user.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Comments } from './comments.entity';

@Entity('posts')
export class Posts {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ nullable: false, length: 50 })
  public title: string;

  @Column({ nullable: false, type: 'text' })
  public content: string;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  public createdOn: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public updatedOn: Date;

  @Column({ nullable: false, default: false })
  public isDeleted: boolean;

  @Column({ nullable: false })
  public userId: number;

  @Column({ nullable: false })
  public username: string;

  @ManyToOne(
    type => User,
    user => user.posts,
    { lazy: true },
  )
  public user: Promise<User>;

  @OneToMany(
    type => Comments,
    comments => comments.post,
    { lazy: true },
  )
  public comments: Promise<Comments[]>;

  @ManyToMany(type => Vote, { eager: true })
  @JoinTable()
  public votes: Vote[];
}
