import { User } from './user.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToOne, CreateDateColumn, JoinColumn } from 'typeorm';

@Entity('bans')
export class Ban {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ nullable: false })
  public banReason: string;

  @Column({ nullable: false })
  public isBanned: boolean;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  public createdOn: Date;

  @OneToOne(
    type => User,
    user => user.bans)
  @JoinColumn()
  public users: User;

  // @CreateDateColumn({
  //   type: 'timestamp',
  //   default: () => 'CURRENT_TIMESTAMP(6)',
  // })
  // public bannedUntil: Date;
}