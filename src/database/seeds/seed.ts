import { Role } from './../entities/role.entity';
import { createConnection, Repository, In } from 'typeorm';
import { UserRole } from '../../users/enums/user-role.enum';
import { User } from '../entities/user.entity';
import * as bcrypt from 'bcrypt';

const seedRoles = async (connection: any) => {
  const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);

  const roles: Role[] = await rolesRepo.find();
  if (roles.length) {
    console.log('The DB already has roles!');
    return;
  }

  const rolesSeeding: Promise<Role>[] = Object.keys(UserRole).map(
    async (roleName: string) => {
      const role: Role = rolesRepo.create({ name: roleName });
      return await rolesRepo.save(role);
    },
  );

  await Promise.all(rolesSeeding);
  console.log('Seeded roles successfully!');
};

const seedAdmin = async (connection: any) => {
  const userRepo: Repository<User> = connection.manager.getRepository(User);
  const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);

  const admin = await userRepo.findOne({
    where: {
      name: 'admin',
    },
  });

  if (admin) {
    console.log('The DB already has an admin!');
    return;
  }

  const roleNames: string[] = Object.keys(UserRole);
  const allUserRoles: Role[] = await rolesRepo.find({
    where: {
      name: In(roleNames),
    },
  });

  if (allUserRoles.length === 0) {
    console.log('The DB does not have any roles!');
    return;
  }

  const username = 'admin';
  const password = 'password123';
  const hashedPassword = await bcrypt.hash(password, 10);

  const newAdmin: User = userRepo.create({
    username,
    password: hashedPassword,
    roles: allUserRoles,
  });

  await userRepo.save(newAdmin);
  console.log('Seeded admin successfully!');
};

const seed = async () => {
  console.log('Seed started!');
  const connection = await createConnection();

  await seedRoles(connection);
  await seedAdmin(connection);

  await connection.close();
  console.log('Seed completed!');
};

seed().catch(console.error);
