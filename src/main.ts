import { ForumSystemErrorFilter } from './common/filters/posts-error.filter';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();
  app.useGlobalPipes(new ValidationPipe({whitelist: true, transform: true }));
  app.useGlobalFilters(new ForumSystemErrorFilter());

  await app.listen(+app.get(ConfigService).get('PORT'));
}
bootstrap();
