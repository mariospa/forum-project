import { AuthService } from './services/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from './common/user';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy{

  private loggedInSubscription: Subscription;
  private userSubscription: Subscription;
  private isAdminSubscription: Subscription;

  public isAdmin: boolean;
  public loggedIn: boolean;
  public user: User;

  constructor(
    private readonly authService: AuthService,
  ) {}

  public ngOnInit() {
    this.loggedInSubscription = this.authService.isLoggedIn$.subscribe(
      loggedIn => this.loggedIn = loggedIn,
    );
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
    this.isAdminSubscription = this.authService.isAdmin$.subscribe(
      isAdmin => this.isAdmin = isAdmin,
    );
  }

  public ngOnDestroy() {
    this.loggedInSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
    this.isAdminSubscription.unsubscribe();
  }

  public logout() {
    this.authService.logoutUser();
  }

}
