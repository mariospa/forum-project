import { CommentVoteComponent } from './comment-vote/comment-vote.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentsComponent } from '../comments/comments.component';
import { CommentsService } from '../services/comments.service';
import { SharedService } from '../services/shared.service';
import { CommentsListComponent } from './comments-list/comments-list.component';
import { CommentCreateComponent } from './comment-create/comment-create.component';

@NgModule({
  declarations: [CommentsComponent, CommentsListComponent, CommentCreateComponent, CommentVoteComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    QuillModule,
  ],
  providers: [CommentsService, SharedService],
  exports: [CommentsComponent, CommentsListComponent],
})
export class CommentsModule {}
