import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-comments-list',
  templateUrl: './comments-list.component.html',
  styleUrls: ['./comments-list.component.css']
})
export class CommentsListComponent implements OnInit {

  @Input() isAdmin;
  @Input() user;
  @Input() title;
  @Input() content;
  @Input() postId;
  @Input() comments;
  @Input() postTitle;
  @Input() postContent;


  config = {
    toolbar: [
      ['bold', 'italic', 'underline'], ['blockquote', 'code-block']
    ]
  }

  @Output() public readonly commentCreateEmitter: EventEmitter<Comment> = new EventEmitter();
  @Output() public readonly updateCommentEmitter: EventEmitter<any> = new EventEmitter();
  @Output() public readonly commentDeleteEmitter: EventEmitter<any> = new EventEmitter();
  @Output() public readonly voteOnPostEmitter: EventEmitter<any>  = new EventEmitter();


  toUpdateComment = false;
  toDeleteComment = false;
  toDeleteId;
  toUpdateId;

  constructor() { }

  ngOnInit(): void {
    
  }

  public createComment(comment): void {
    this.commentCreateEmitter.emit(comment);
  }

  public update(commentId) {
    this.updateCommentEmitter.emit({ title: this.title, content: this.content, id: commentId, postId: this.postId });
  }

  public delete(commentId) {
    this.commentDeleteEmitter.emit({ id: commentId, postId: this.postId });
  }

  updateCommentToggle(id) {
    this.toUpdateComment = !this.toUpdateComment;
    this.toUpdateId = id;
  }

  assignContent(content) {
    this.content = content;
  }

  assignTitle(title) {
    this.title = title;
  }

  commentVoteToggle(id) {
    this.voteOnPostEmitter.emit(id);
  }
}
