import { AuthService } from '../services/auth.service';
import { Subscription } from 'rxjs';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Component, OnInit, DoCheck, Input, OnDestroy } from '@angular/core';
import { CommentsService } from '../services/comments.service';
import { SharedService } from '../services/shared.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css'],
})
export class CommentsComponent implements OnInit, OnDestroy {

  postId;
  comments;
  content;
  title;
  user;
  likedFromUser;
  public isAdmin: boolean;
  public searchOptions = {};
  public commentsCount: number;
  public commentsPerPage = 5;
  public page = 1;
  public pages;
  public currentPage;

  @Input() postTitle: string;
  @Input() postContent: string;

  private isAdminSubscription: Subscription;
  private userSubscription: Subscription;

  constructor(
    private readonly commentsService: CommentsService,
    private readonly sharingService: SharedService,
    private readonly flashMessagesService: FlashMessagesService,
    private readonly authService: AuthService,
    private readonly route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.postId = +params.get('id');
    });

    this.countComments(this.postId, {});

    this.loadComments(this.postId, {limit: this.commentsPerPage});
    // this.loadPosts({ title: this.searchText, limit: this.postsPerPage });

    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
    this.isAdminSubscription = this.authService.isAdmin$.subscribe(
      isAdmin => this.isAdmin = isAdmin,
    );
  }
 
  public countComments(id, options): void {
    this.commentsService.countComments(id, options).subscribe(
      (data: number) => {
        this.commentsCount = data;
        this.pages = Math.ceil(this.commentsCount / this.commentsPerPage);
      });
  }

  public loadComments(postId, options): void {
    this.commentsService.all(postId, options).subscribe(
      (data: any) => {
        this.comments = data.map(comment => {
          const liked = comment.votes.filter(el => el.liked === true).length;

          if (comment.votes.length && this.user) {
            const voteByUser = comment.votes.filter(el => el.username === this.user.username);
            if (voteByUser.length && voteByUser[0].liked) {
              this.likedFromUser = true;
            } else {
              this.likedFromUser = false;
            }
          } else {
            this.likedFromUser = false;
          }

          return {
            ...comment,
            likes: liked,
            likedByUser: this.likedFromUser,
          };
        });
      });
  }

  createComment(comment) {
    this.commentsService.createComment(comment, this.postId).subscribe(
      (res) => {
        this.comments.push(res);
        this.flashMessagesService.show('Comment created!', { cssClass: 'alert-success' });
      },
      (err) => {
        this.flashMessagesService.show('Wrong!', { cssClass: 'alert-success' });
      }
    );
  }

  updateComment(comment) {
    this.commentsService.updateComment(comment).subscribe(
      (res: any) => {
        this.flashMessagesService.show('Content updated!', { cssClass: 'alert-success' });
        this.comments.forEach(el => {
          if (el.id == res.id) {
            el.title = res.title;
            el.content = res.content;
          }
        })
      },
      (err) => console.log(err)
    )
  }

  deleteComment(comment) {
    this.commentsService.deleteComment(comment).subscribe(
      (res: any) => {
        this.comments = this.comments.filter(el => {
          return el.id !== res.id;
        });
        this.flashMessagesService.show('Content Deleted!', { cssClass: 'alert-success' });
      },
      (err) => console.log(err)
    )
  }

  addCommentVote(id) {
    this.commentsService.likeComment(this.postId, id).subscribe(
      (res: any) => {
        if (res.liked) {
          const index: number = this.comments.findIndex(
            (post) => post.id === id
          );

          this.comments[index].votes = res;
          this.comments[index].likedByUser = true;
          this.comments[index].likes++;
        } else {
          const index: number = this.comments.findIndex(
            (post) => post.id === id
          ); 

          this.comments[index].votes = res;
          this.comments[index].likedByUser = false;
          this.comments[index].likes--;
        }
      },
      (err) => console.log(err)
    );
  }

  jumpToPage(page) {
    this.page = page;
    this.loadComments(this.postId, {
        skip: this.commentsPerPage * (this.page - 1),
        limit: this.commentsPerPage
      });
  }

  public ngOnDestroy() {
    this.userSubscription.unsubscribe();
    this.isAdminSubscription.unsubscribe();
  }
}
