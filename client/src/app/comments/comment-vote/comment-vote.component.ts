import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-comment-vote',
  templateUrl: './comment-vote.component.html',
  styleUrls: ['./comment-vote.component.css']
})
export class CommentVoteComponent {
  @Input() user;
  @Input() post;

  @Output() public readonly voteOnPostEmitter = new EventEmitter<any>();
  @Output() public readonly likesEmitter: EventEmitter<any> = new EventEmitter();

  constructor() { }

  postVoteToggle(id) {
    this.voteOnPostEmitter.emit(id);
  }

}
