import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Comment } from './../../common/comment';


@Component({
  selector: 'app-comment-create',
  templateUrl: './comment-create.component.html',
  styleUrls: ['./comment-create.component.css']
})
export class CommentCreateComponent implements OnInit {

  form: FormGroup;
  @Input() title;
  @Input() content;
  @Input() postTitle;
  @Input() postContent;

  @Output() public readonly commentCreate: EventEmitter<Comment> = new EventEmitter();

  config = {
    toolbar: [
      ['bold', 'italic', 'underline'], ['blockquote', 'code-block']
    ]
  }

  constructor( 
    private readonly formBuilder: FormBuilder,
    private readonly flashMessagesService: FlashMessagesService,
    ) { }

  ngOnInit(): void {
    this.createForm();
    
  }

  createForm() {
    this.form = this.formBuilder.group({
      title: [
        `Re: ${this.postTitle}`,
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(35),
        ]),
      ],
      content: [
        `<blockquote>${this.postContent}</blockquote><br><br>`,
        Validators.compose([
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(500),
        ]),
      ],
    });
  }


  public onCreateButtonClick(ti, co): void {
    const comment = {
      title: ti,
      content: co,
    };
    this.commentCreate.emit(comment);
  }

}
