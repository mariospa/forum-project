import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from '../services/auth.service';
import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(
    private readonly injector: Injector,
    private readonly flashMessagesService: FlashMessagesService,
    private readonly router: Router,
  ) { }

  intercept(req, next) {
    const authService = this.injector.get(AuthService);
    const tokenizedReq = req.clone({
      setHeaders: {
        Authorization: `Bearer ${authService.getToken()}`
      }
    });
    return next.handle(tokenizedReq)
      .pipe(
        catchError(err => {
          if (err.error.error === 'Unauthorized') {
            this.flashMessagesService.show(`${err.error.error}`, { cssClass: 'alert-danger' });
            localStorage.clear();
            this.router.navigate(['/users/login']);
          }
          return throwError(err);
        }),
        // Activity Log
        // finalize(() => {
        //   const profilingMsg = `${req.method} ${req.urlWithParams}`;
        //   console.log(profilingMsg);
        // })
      );
  }
}
