import { Router } from '@angular/router';
import { AuthGuard } from './../../auth.guard';
import { FlashMessagesService } from 'angular2-flash-messages';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-users-login',
  templateUrl: './users-login.component.html',
  styleUrls: ['./users-login.component.css']
})
export class UsersLoginComponent implements OnInit {

  postForm;
  previousUrl;
  processing = false;
  form;
  // helper = new JwtHelperService();
  // decodedToken: any;

  constructor(
    private AuthService: AuthService,
    private formBuilder: FormBuilder,
    private flashMessagesService: FlashMessagesService,
    private authGuard: AuthGuard,
    private router: Router,
  ) {
    this.createForm();
  }

  createForm() {
    this.form = this.formBuilder.group({
      username: ['', Validators.compose([
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(15),
      this.validateUsername
    ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(35),
        this.validatePassword
      ])]
    });
  }

  disableForm() {
    this.form.controls['username'].disable();
    this.form.controls['password'].disable();
  }

  enableForm() {
    this.form.controls['username'].enable();
    this.form.controls['password'].enable();
  }

  validateUsername(controls) {
    // Create a regular expression
    const regExp = new RegExp(/^[a-zA-Z0-9]+$/);
    // Test username against regular expression
    if (regExp.test(controls.value)) {
      return null; // Return as valid username
    } else {
      return { 'validateUsername': true };
    }
  }

  validatePassword(controls) {
    const regExp = new RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/);
    if (regExp.test(controls.value)) {
      return null;
    } else {
      return { 'validatePassword': true };
    }
  }

  ngOnInit(): void {
    if (this.authGuard.redirectUrl) {
      this.flashMessagesService.show('You must be looged in to view that page', { cssClass: 'alert-danger' });
      this.previousUrl = this.authGuard.redirectUrl;
      this.authGuard.redirectUrl = undefined;
    }
  }

  onSubmit(customerData) {
    // Process checkout data here
    this.postForm.reset();
  }

  public loginUser() {
    this.processing = true;
    this.disableForm();


    const user = {
      username: this.form.get('username').value,
      password: this.form.get('password').value,
    };

    this.AuthService.loginUser(user)
      .subscribe(
        (res: any) => {
          if (this.previousUrl) {
            this.router.navigate([this.previousUrl]);
          } else {
            this.router.navigate(['/']);
          }
        },
        (err: any) => {
          this.processing = false; // Enable submit button
          this.enableForm(); // Enable form for editting
        },
        () => console.log('HTTP request completed.')
      );
  }
}
