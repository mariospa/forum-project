import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from './../services/user.service';
import { User } from './../common/user';
import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

@Injectable()
export class CurrentUserResolverService implements Resolve<User> {

  constructor(
    private readonly router: Router,
    private readonly userService: UserService,
    private readonly flashMessagesService: FlashMessagesService,
  ) { }

  resolve() {
    if (localStorage.getItem('token')) {
      return this.userService.getCurrentUser()
        .pipe(
          map(
            (user: User) => user,
            () => false,
          )
        );
    } else {
      return null;
    }
  }
}
