import { QuillModule } from 'ngx-quill';
import { CurrentUserResolverService } from './current-user-resolver.service';
import { UsersResolverService } from './users-resolver.service';
import { UsersRouterModule } from './users-router.module';
import { PostsService } from './../services/posts.service';
import { PostsModule } from './../posts/posts.module';
import { UsersComponent } from './users.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { UsersListComponent } from './users-list/users-list.component';
import { UsersLoginComponent } from './users-login/users-login.component';
import { SingleUserComponent } from './single-user/single-user.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { UserProfileComponent } from './user-profile/user-profile.component';

@NgModule({
  declarations: [
    UsersComponent,
    UsersListComponent,
    UsersLoginComponent,
    SingleUserComponent,
    CreateUserComponent,
    UserProfileComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    PostsModule,
    UsersRouterModule,
    QuillModule.forRoot(),
  ],
  providers: [PostsService, UsersResolverService, CurrentUserResolverService],
  exports: [UsersListComponent],
})
export class UsersModule { }
