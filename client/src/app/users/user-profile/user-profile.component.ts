import { concat } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  user;
  editProfile = false;
  form: FormGroup;

  constructor(
    private readonly userService: UserService,
    private readonly AuthService: AuthService,

    private formBuilder: FormBuilder,
    private flashMessagesService: FlashMessagesService,
    private router: Router,
  ) {
    this.createForm();
  }

  createForm() {
    this.form = this.formBuilder.group({
      // Username Input
      username: ['', Validators.compose([
        Validators.minLength(2), // Minimum length is 3 characters
        Validators.maxLength(15), // Maximum length is 15 characters
        this.validateUsername // Custom validation
      ])],
      // Password Input
      password: ['', Validators.compose([
        Validators.minLength(5), // Minimum length is 5 characters
        Validators.maxLength(35), // Maximum length is 35 characters
        this.validatePassword // Custom validation
      ])],
      // Confirm Password Input
      confirm: ['', Validators.required] // Field is required
    }, { validator: this.matchingPasswords('password', 'confirm') }); // Add custom validator to form for matching passwords
  }

  validateUsername(controls) {
    // Create a regular expression
    const regExp = new RegExp(/^[a-zA-Z0-9]+$/);
    // Test username against regular expression
    if (regExp.test(controls.value)) {
      return null; // Return as valid username
    } else {
      return { 'validateUsername': true } // Return as invalid username
    }
  }

  validatePassword(controls) {
    // Create a regular expression
    //^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[\d])(?=.*?[\W]).{6,35}$/

    const regExp = new RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/);
    // Test password against regular expression
    if (regExp.test(controls.value)) {
      return null; // Return as valid password
    } else {
      return { 'validatePassword': true } // Return as invalid password
    }
  }

  matchingPasswords(password, confirm) {
    return (group: FormGroup) => {
      // Check if both fields are the same
      if (group.controls[password].value === group.controls[confirm].value) {
        return null; // Return as a match
      } else {
        return { 'matchingPasswords': true } // Return as error: do not match
      }
    }
  }

  onRegisterSubmit() {
    console.log('form submitted');
  }

  editProfileToggle() {
    this.editProfile = !this.editProfile;
  }

  ngOnInit(): void {
    this.userService.getCurrentUser()
      .subscribe(
        (res: any) => this.user = res,
      );
  }

  updateUser(name, pass) {
    this.user = {
      ...this.user,
      username: name,
      password: pass,
    };

    const update = this.userService.updateUser(this.user, this.user.id);

    const login = this.AuthService.loginUser(this.user);

    concat(update, login).subscribe(
      value => console.log(value),
      err => { },
      () => console.log('...and it is done!')
    );
  }

  upload(event, user, avatarUrl) {
    const file = event.target.files[0];
    this.userService.upload(file, user.id, avatarUrl).subscribe(
      (res: any) => user.avatarUrl = res.avatarUrl,
      (err) => console.log(err),
    );
  }
}
