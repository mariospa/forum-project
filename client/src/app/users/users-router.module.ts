import { UsersComponent } from './users.component';
import { CurrentUserResolverService } from './current-user-resolver.service';
import { UsersResolverService } from './users-resolver.service';
import { AuthGuard } from './../auth.guard';
import { UsersListComponent } from './users-list/users-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { SingleUserComponent } from './single-user/single-user.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { UsersLoginComponent } from './users-login/users-login.component';

const usersRoutes: Routes = [
  { path: '', component: UsersComponent,
  resolve: { users: UsersResolverService, user: CurrentUserResolverService}, pathMatch: 'full' },
  { path: 'create', component: CreateUserComponent },
  { path: 'login', component: UsersLoginComponent },
  {
    path: 'profile',
    component: UserProfileComponent,
    canActivate: [AuthGuard],
  },
  { path: ':id', component: SingleUserComponent },


 
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(usersRoutes),
  ],
  exports: [
    RouterModule,
  ],
})

export class UsersRouterModule { }
