import { AuthService } from './../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from './../services/user.service';
import { User } from './../common/user';
import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';



@Injectable({
  providedIn: 'root'
})

@Injectable()
export class UsersResolverService implements Resolve<User[]> {

  constructor(
    private readonly router: Router,
    private readonly userService: UserService,
    private readonly authService: AuthService,
    private readonly flashMessagesService: FlashMessagesService,
  ) { }

  resolve() {
    return this.userService.getUsers()
      .pipe(
        map(users => {
          if (users) {
            return users;
          } else {
            this.router.navigate(['/home']);
            this.flashMessagesService.show('Smth went wrong!', { cssClass: 'alert-alert' });
            return;
          }
        })
      );

  }
}
