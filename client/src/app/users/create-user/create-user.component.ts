import { AuthService } from '../../services/auth.service';
import { UserService } from './../../services/user.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { take, debounceTime, map, catchError, finalize, flatMap } from 'rxjs/operators';
import { of, throwError, Observable, concat } from 'rxjs';


const nameAsyncValidator = (service) => (controls: FormControl) => {
  if (!controls || String(controls.value).length === 0) {
    return of(null);
  }

  return service.getUserByName(controls.value)
    .pipe(
      catchError(() => of([])),
      debounceTime(5000),
      take(1),
      map(
        (arr: any) => arr.length ? { 'usernameExists': true } : null,
      )
    );
}

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  form: FormGroup;
  user;
  keyword = '';
  userNameExists = false;

  constructor(
    private readonly userService: UserService,
    private formBuilder: FormBuilder,
    private flashMessagesService: FlashMessagesService,
    private router: Router,
    private AuthService: AuthService,
  ) {
    this.createForm();
  }

  go = 'asdf';
 
  createForm() {
    this.form = this.formBuilder.group({
      // Username Input
      username: ['', Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(15),
        this.validateUsername,
      ]), [nameAsyncValidator(this.userService)]],
      // Password Input
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(35),
        this.validatePassword
      ])],
      // Confirm Password Input
      confirm: ['', Validators.required]
    }, { validator: this.matchingPasswords('password', 'confirm') });
  }

  validateUsername(controls) {
    // Create a regular expression
    const regExp = new RegExp(/^[a-zA-Z0-9]+$/);
    // Test username against regular expression
    if (regExp.test(controls.value)) {
      return null; // Return as valid username
    } else {
      return { 'validateUsername': true };
    }
  }

  validatePassword(controls) {
    const regExp = new RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/);
    if (regExp.test(controls.value)) {
      return null;
    } else {
      return { 'validatePassword': true };
    }
  }

  matchingPasswords(password, confirm) {
    return (group: FormGroup) => {
      if (group.controls[password].value === group.controls[confirm].value) {
        return null;
      } else {
        return { 'matchingPasswords': true };
      }
    };
  }

  onRegisterSubmit() {
    console.log('form submitted');
  }

  ngOnInit(): void {

  }

  public addUser(name, pass) {
    this.user = {
      username: name,
      password: pass,
    };

    const register = this.userService.addUser(this.user);

    const login = this.AuthService.loginUser(this.user);
  
    concat(register, login).subscribe(
      value => console.log(value),
      err => {},
      () => console.log('...and it is done!')
    );

    this.router.navigate(['/']);
  }
}




