import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from './../common/user';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  friends;
  users: User[];
  loggedUser: User;

  config = {
    toolbar: [
      ['bold', 'italic', 'underline'], ['blockquote', 'code-block']
    ]
  }

  public isAdmin: boolean;

  private isAdminSubscription: Subscription;

  constructor(
    private readonly flashMessagesService: FlashMessagesService,
    private readonly userService: UserService,
    private readonly route: ActivatedRoute,
    private readonly authService: AuthService,
  ) { }

  ngOnInit(): void {
    // this.friends = this.route.snapshot.data['friends'];
    this.users = this.route.snapshot.data['users'];
    this.loggedUser = this.route.snapshot.data['user'];

    this.isAdminSubscription = this.authService.isAdmin$.subscribe(
      isAdmin => this.isAdmin = isAdmin,
    );
  }

  banUser(user) {
    this.userService.banUser(user).subscribe(
      () => {
        const foundUser = this.users.find(el => el.id === user.id);
        foundUser.bans = true;
        this.flashMessagesService.show('User banned!', { cssClass: 'alert-warning' });
      },
      (err) => console.log(err)
    )
  }

  unbanUser(user) {
    this.userService.unbanUser(user).subscribe(
      () => {
        const foundUser = this.users.find(el => el.id === user.id);
        foundUser.bans = false;
        this.flashMessagesService.show('User unbanned!', { cssClass: 'alert-warning' });
      },
      (err) => console.log(err)
    )
  }

  public ngOnDestroy() {
    this.isAdminSubscription.unsubscribe();
  }

}
