import { FlashMessagesService } from 'angular2-flash-messages';
import { map, finalize } from 'rxjs/operators';
import { UserService } from './../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { User } from './../../common/user';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
  @Input() loggedUser: User;
  @Input() users: User[];
  @Input() isAdmin: boolean;

  @Output() public readonly banUserEmitter: EventEmitter<User> = new EventEmitter();
  @Output() public readonly unbanUserEmitter: EventEmitter<User> = new EventEmitter();

  constructor(
    private readonly flashMessagesService: FlashMessagesService,
    private readonly userService: UserService,
  ) { }

  ngOnInit(): void {
  }

  addFriend(user) {
    this.userService.addFriend(user)
      .subscribe(
        (res: any) => {
          this.loggedUser = res;
          this.flashMessagesService.show('Befriended!', { cssClass: 'alert-success' });
        },
        (err) => console.log(err),
      );
  }

  removeFriend(user) {
    this.userService.removeFriend(user)
      .subscribe(
        (res: any) => {
          this.loggedUser = res;
          this.flashMessagesService.show('Unfriended!', { cssClass: 'alert-danger' })
        },
        (err) => console.log(err),
      );
  }

  public banUser(user) {
    this.banUserEmitter.emit(user);
  }

  public unbanUser(user) {
    this.unbanUserEmitter.emit(user);
  }


  amFriendsWith(user) {
    return this.loggedUser.friends.includes(user.username);
  }

  test(value) {
    console.log(value)
  }
}
