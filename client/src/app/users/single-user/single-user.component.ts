import { forkJoin } from 'rxjs';
import { PostsService } from './../../services/posts.service';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-single-user',
  templateUrl: './single-user.component.html',
  styleUrls: ['./single-user.component.css']
})
export class SingleUserComponent implements OnInit {

  friends;
  user;
  postsOfUser;
  selectedId: number;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly userService: UserService,
    private readonly postsService: PostsService,
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(
      params => {
        this.selectedId = +params.get('id');
        console.log(this.selectedId);
      }
    );

    this.userService.getUser(this.selectedId)
      .subscribe(
        (data) => {
          this.user = data;
          this.postsOfUser = this.user.__posts__;
          console.log(this.user)
          this.postsService.getPostsOfUser(this.user.id);
        },
      );
  }
}
