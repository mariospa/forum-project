import { AuthService } from '../../services/auth.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  @Input() isAdmin: boolean;
  @Input() loggedIn: boolean;
  @Input() user;

  @Output() logout = new EventEmitter<void>();

  constructor(public authService: AuthService){
  }

  public logoutTrigger() {
    this.logout.emit();
  }
}