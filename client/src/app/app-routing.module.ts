import { UsersModule } from './users/users.module';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { CommentsComponent } from './comments/comments.component';
import { PostCreateComponent } from './posts/post-create/post-create.component';
import { PostsComponent } from './posts/posts.component';
import { SinglePostComponent } from './posts/single-post/single-post.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/posts', pathMatch: 'full' },
  { path: 'posts', component: PostsComponent },
  {
    path: 'post/:id',
    component: SinglePostComponent,
    children: [{ path: 'comments', component: CommentsComponent }],
  },
  { path: 'create', component: PostCreateComponent },
  { path: 'users', loadChildren: () => import('./users/users.module').then(m => m.UsersModule)},

  { path: 'not-found', component: NotFoundComponent },
  { path: '**', redirectTo: '/not-found' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
