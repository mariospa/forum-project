import { JwtModule } from '@auth0/angular-jwt';
import { SpinnerIntercerptorService } from './interceptors/spinner-interceptor.service';
import { TokenInterceptorService } from './interceptors/token-interceptor.service';
import { AuthGuard } from './auth.guard';
import { FooterComponent } from './components/footer/footer.component';
import { RouterModule } from '@angular/router';
import { PostsModule } from './posts/posts.module';
import { HeaderComponent } from './components/header/header.component';
import { MaterialModule } from './material/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import {
  FlashMessagesModule,
  FlashMessagesService,
} from 'angular2-flash-messages';
import { NgxSpinnerModule, NgxSpinnerService } from 'ngx-spinner';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { CommentsModule } from './comments/comments.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NotFoundComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    PostsModule,
    CommentsModule,
    RouterModule,
    FlashMessagesModule,
    NgxSpinnerModule,
    JwtModule.forRoot({ config: {} }),

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    AuthGuard,
    FlashMessagesService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerIntercerptorService,
      multi: true,
    },
    NgxSpinnerService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
