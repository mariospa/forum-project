export interface Posts {

    id: number;

    title: string;

    content: string;

    createdOn: Date;

    updatedOn: Date;

    votes: any[];

    likes?: number;

    likedByUser?: boolean;

    userId: number;

}
