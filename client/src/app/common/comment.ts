export interface Comment {
    title: string;

    content: string;
}