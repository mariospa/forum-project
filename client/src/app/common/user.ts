export interface User {

    id: number;

    username: string;

    friends: User[];

    bans: boolean;

    avatarUrl: string;

}
