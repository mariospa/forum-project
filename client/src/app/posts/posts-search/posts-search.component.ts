import { Component, Output, EventEmitter, DoCheck } from '@angular/core';

@Component({
  selector: 'app-posts-search',
  templateUrl: './posts-search.component.html',
  styleUrls: ['./posts-search.component.css'],
})
export class PostsSearchComponent implements DoCheck {
  public keyword = '';

  @Output() search = new EventEmitter<string>();

  public triggerSearch(value) {
    this.search.emit(value);
  }
  ngDoCheck() {
  }
}
