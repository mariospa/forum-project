import { CommentsService } from './../../services/comments.service';

import { Posts } from './../../common/posts';
import { Component, OnInit, Input, DoCheck } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { PostsService } from './../../services/posts.service';
import { FormBuilder } from '@angular/forms';
import { SharedService } from './../../services/shared.service';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.css'],
})
export class SinglePostComponent implements OnInit {
  post: Posts;
  selectedId: number;
  liked: number;
  postForm;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private readonly postsService: PostsService,
  ) {
    this.postForm = this.formBuilder.group({
      co: '',
    });
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.selectedId = +params.get('id');
    });
    this.postsService
      .getPost(this.selectedId)
      .subscribe((data: Posts) => {
        const liked = data.votes.filter(el => el.liked === true).length;
        this.post = data;
        data.likes = liked;
      });

  }
}
