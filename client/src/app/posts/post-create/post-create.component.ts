import { FlashMessagesService } from 'angular2-flash-messages';
import { PostsService } from './../../services/posts.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css'],
})
export class PostCreateComponent implements OnInit {
  form: FormGroup;

  config = {
    toolbar: [
      ['bold', 'italic', 'underline'], ['blockquote', 'code-block']
    ]
  }

  constructor(
    private readonly postsService: PostsService,
    private formBuilder: FormBuilder,
    private readonly flashMessagesService: FlashMessagesService,
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  public addPost(title, content) {
    console.log(content)
    this.postsService.addPost({ title, content }).subscribe(
      (res) => {
        console.log(res);
        this.flashMessagesService.show('You created a message', { cssClass: 'alert-success' })
      },
      (err) => console.log(err)
    );
  }

  createForm() {
    this.form = this.formBuilder.group({
      title: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(35),
        ]),
      ],
      content: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(500),
        ]),
      ],
    });
  }
}
