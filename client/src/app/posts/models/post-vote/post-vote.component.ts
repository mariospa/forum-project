import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-post-vote',
  templateUrl: './post-vote.component.html',
  styleUrls: ['./post-vote.component.css']
})
export class PostVoteComponent {
  @Input() user;
  @Input() post;

  @Output() public readonly voteOnPostEmitter = new EventEmitter<any>();
  @Output() public readonly likesEmitter: EventEmitter<any> = new EventEmitter();

  constructor() { }

  postVoteToggle(id) {
    this.voteOnPostEmitter.emit(id);
  }

}
