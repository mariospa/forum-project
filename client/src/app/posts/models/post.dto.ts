export class PostDTO {
    id: number;
    title: string;
    content: string;
}