import { QuillModule } from 'ngx-quill';
import { RouterModule } from '@angular/router';
import { PostsListComponent } from './posts-list/posts-list.component';
import { PostsSearchComponent } from './posts-search/posts-search.component';
import { PostsService } from '../services/posts.service';
import { PostsComponent } from './posts.component';
import { MaterialModule } from './../material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SinglePostComponent } from './single-post/single-post.component';
import { PostCreateComponent } from './post-create/post-create.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedService } from '../services/shared.service';
import { CommentsModule } from '../comments/comments.module';
import { PostVoteComponent } from './models/post-vote/post-vote.component';

@NgModule({
  declarations: [
    PostsComponent,
    SinglePostComponent,
    PostsSearchComponent,
    PostsListComponent,
    PostCreateComponent,
    PostVoteComponent,
  ],
  imports: [
    CommentsModule,
    CommonModule,
    MaterialModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    QuillModule.forRoot(),
  ],
  providers: [PostsService, SharedService],
  exports: [
    PostsComponent,
    SinglePostComponent,
    PostCreateComponent,
    PostsListComponent,
  ],
})
export class PostsModule {}
