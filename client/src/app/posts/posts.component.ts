import { Vote } from './../common/vote';
import { User } from './../common/user';
import { AuthService } from '../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { PostsService } from '../services/posts.service';
import { Posts } from '../common/posts';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable, Subject } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],
})
export class PostsComponent implements OnInit, OnDestroy {
  public posts = [];
  public activePost: Posts = null;
  public user: User;
  public isAdmin: boolean;
  public postVote: Vote;
  public searchOptions = {};
  public likedFromUser: boolean;
  public postsCount: number;
  public postsPerPage = 5;
  public page = 1;
  public pages;
  public searchText;
  public currentPage;

  public results$: Observable<any>;
  public subject = new Subject();

  private userSubscription: Subscription;
  private isAdminSubscription: Subscription;

  constructor(
    private readonly postsService: PostsService,
    private readonly flashMessagesService: FlashMessagesService,
    private readonly authService: AuthService,
  ) { }

  ngOnInit() {
    this.loadPosts({ limit: this.postsPerPage });
    this.countPosts({});
    this.loadUserSubscription();
    this.loadAdminSubscription();

    this.subject.pipe(
      debounceTime(1000),
      map(searchText => this.searchText = searchText
      )
    ).subscribe(
      () => {
        this.page = 1;
        this.countPosts({ title: this.searchText });
        this.loadPosts({ title: this.searchText, limit: this.postsPerPage });
      },
      () => console.log('error'))
  }

  public selectPost(post: Posts) {
    this.activePost = post;
  }

  jumpToPage(page) {
    this.page = page;
    this.loadPosts({
        title: this.searchText,
        skip: this.postsPerPage * (this.page - 1),
        limit: this.postsPerPage
      });
  }

  pushToSearchSubject(searchText): void {
    this.subject.next(searchText);
  }

  updatePost(comment): void {
    this.postsService.updatePost(comment).subscribe(
      (res: any) => {
        this.flashMessagesService.show('Content updated!', { cssClass: 'alert-success' });
        this.posts.forEach(el => {
          if (el.id === res.id) {
            el.title = res.title;
            el.content = res.content;
          }
        });
      },
      (err) => console.log(err)
    );
  }

  deletePost(comment): void {
    this.postsService.deletePost(comment).subscribe(
      (res: any) => {
        this.posts = this.posts.filter(el => {
          return el.id !== res.id;
        });
        this.flashMessagesService.show('Content Deleted!', { cssClass: 'alert-success' });
      },
      (err) => console.log(err)
    );
  }

  addPostVote(id): void {
    this.postsService.likePost(id).subscribe(
      (res: any) => {
        if (res.liked) {
          const index: number = this.posts.findIndex(
            (post) => post.id === id
          );

          this.posts[index].votes = res;
          this.posts[index].likedByUser = true;
          this.posts[index].likes++;
        } else {
          const index: number = this.posts.findIndex(
            (post) => post.id === id
          );

          this.posts[index].votes = res;
          this.posts[index].likedByUser = false;
          this.posts[index].likes--;
        }
      },
      (err) => console.log(err)
    );
  }

  public countPosts(options): void {
    this.postsService.countPosts(options).subscribe(
      (data: number) => {
        this.postsCount = data;
        this.pages = Math.ceil(this.postsCount / this.postsPerPage);
      });
  }

  public createOptions(value) {
    const options = {
      isDeleted: false,
      title: value,
      // ...(value.title && { title: value.title }),
      // ...(value.content && { content: value.content }),
      // ...(value.limit && { limit: value.limit }),
      // ...(value.skip && { skip: value.skip }),
    }
    this.loadPosts(options);
  }

  public loadPosts(searchOptions): void {
    this.postsService.all(searchOptions).subscribe(
      (data: Posts[]) => {
        this.posts = data.map(post => {
          const liked = post.votes.filter(el => el.liked === true).length;

          if (post.votes.length && this.user) {
            const voteByUser = post.votes.filter(el => el.username === this.user.username);
            if (voteByUser.length && voteByUser[0].liked) {
              this.likedFromUser = true;
            } else {
              this.likedFromUser = false;
            }
          } else {
            this.likedFromUser = false;
          }

          return {
            ...post,
            likes: liked,
            likedByUser: this.likedFromUser,
          };
          
        });
        console.log(data)
      });
  }

  public loadUserSubscription(): void {
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
  }

  public loadAdminSubscription(): void {
    this.isAdminSubscription = this.authService.isAdmin$.subscribe(
      isAdmin => this.isAdmin = isAdmin,
    );
  }

  public ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
    this.isAdminSubscription.unsubscribe();
  }
}
