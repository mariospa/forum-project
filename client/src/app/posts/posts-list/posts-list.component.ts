import { Posts } from './../../common/posts';
import { Component, Input, Output, EventEmitter, DoCheck, OnInit } from '@angular/core';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.css'],
})
export class PostsListComponent implements OnInit {
  @Input() user;
  @Input() isAdmin: boolean;
  @Input() posts: Posts[];
  @Input() selectedPost: Posts;
  @Input() postVote;
  @Input() postsCount: number;

  @Output() public readonly selectPost = new EventEmitter<Posts>();
  @Output() public readonly voteOnPostEmitter = new EventEmitter<any>();
  @Output() public readonly updatePostEmitter: EventEmitter<any> = new EventEmitter();
  @Output() public readonly deletePostEmitter: EventEmitter<any> = new EventEmitter();

  toUpdatePost = false;
  toDeletePost = false;
  toDeleteId;
  toUpdateId;
  title;
  content;
  votes;
  postsPerPage = 13;
  limit = 13;

  p = 1;

  config = {
    toolbar: [
      ['bold', 'italic', 'underline'], ['blockquote', 'code-block']
    ]
  }

  ngOnInit() {
  }

  public triggerSelectPost(post: Posts) {
    this.selectPost.emit(post);
  }

  public update(postId: number) {
    this.updatePostEmitter.emit({ title: this.title, content: this.content, id: postId });
  }

  public delete(postId: number) {
    this.deletePostEmitter.emit(postId);
  }

  updatePostToggle(id) {
    this.toUpdatePost = !this.toUpdatePost;
    this.toUpdateId = id;
  }

  postVoteToggle(id) {
    this.voteOnPostEmitter.emit(id);
  }

  showPagination() {
    if (this.posts.length / 10 >= 1) {
      return true;
    } else {
      return false;
    }
  }

  assignContent(content) {
    this.content = content;
  }

  assignTitle(title) {
    this.title = title;
  }

  refactorHtmlToText(html) {
    const temp = document.createElement('div');
    temp.innerHTML = html;
    return temp.textContent || temp.innerText || '';
  }
}
