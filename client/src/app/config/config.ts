const DOMAIN_NAME = 'http://localhost:3000';
const USER_AVATAR_PREFIX = `${DOMAIN_NAME}/avatars/`;
const DEFAULT_USER_AVATAR = `${DOMAIN_NAME}/avatars/dummy.jpg`;

export const CONFIG = {
    DOMAIN_NAME,
    USER_AVATAR_PREFIX,
    DEFAULT_USER_AVATAR
};
