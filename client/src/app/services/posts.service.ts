import { Vote } from './../common/vote';
import { User } from './../common/user';
import { Posts } from './../common/posts';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CreatePostDTO } from '../posts/models/create-post.dto';
import { CONFIG } from './../config/config';
import { PostDTO } from '../posts/models/post.dto';
import { filter } from 'rxjs/operators';


@Injectable({
  providedIn: 'root',
})
export class PostsService {

  constructor(private readonly http: HttpClient) { }

  public all(searchOptions): Observable<Posts[]> {
    const options = {};
    Object.assign(options,
      searchOptions.limit && { limit: searchOptions.limit },
      searchOptions.skip && { skip: searchOptions.skip },
      searchOptions.title && { title: searchOptions.title },
      searchOptions.content && { content: searchOptions.content },
      searchOptions.deleted && { deleted: searchOptions.deleted },
    );

    return this.http.get<Posts[]>(`${CONFIG.DOMAIN_NAME}/posts`, {
      params: {
        ...options,
      },
    });
  }

  public getPost(id: number): Observable<Posts> {
    return this.http.get<Posts>(`${CONFIG.DOMAIN_NAME}/posts/${id}`);
  }

  public countPosts(searchOptions): Observable<number> {
    const options = {};

    Object.assign(options,
      searchOptions.title && { title: searchOptions.title },
      searchOptions.content && { content: searchOptions.content },
      searchOptions.deleted && { deleted: searchOptions.deleted },
    );
    return this.http.get<number>(`${CONFIG.DOMAIN_NAME}/posts/count`, {
      params: {
        ...options,
      }
    });
  }

  public addPost(post: CreatePostDTO): Observable<Partial<Posts>> {
    return this.http.post<CreatePostDTO>(`${CONFIG.DOMAIN_NAME}/post`, post);
  }

  public getPostsOfUser(user: User): Observable<Partial<Posts[]>> {
    return this.http.get<Posts[]>(`${CONFIG.DOMAIN_NAME}/posts`, {
      params: { author: `${user.username}` },
    });
  }

  public updatePost(content: PostDTO): Observable<Partial<Posts>> {
    return this.http.put<PostDTO>(`${CONFIG.DOMAIN_NAME}/posts/${content.id}`, content);
  }

  public deletePost(id: number): Observable<PostDTO> {
    return this.http.delete<PostDTO>(`${CONFIG.DOMAIN_NAME}/posts/${id}`);
  }

  public likePost(id): Observable<Vote> {
    return this.http.put<Vote>(`${CONFIG.DOMAIN_NAME}/posts/${id}/like`, {});
  }

}
