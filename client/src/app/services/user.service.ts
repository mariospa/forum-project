import { User } from './../common/user';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError, ObjectUnsubscribedError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private readonly http: HttpClient,
    private readonly flashMessagesService: FlashMessagesService,
  ) { }

  public getUser(id: number) {
    return this.http.get(`http://localhost:3000/users/${id}`);
  }

  public getCurrentUser() {
    return this.http.get(`http://localhost:3000/user`);
  }

  public getUsers() {
    return this.http.get<User[]>(`http://localhost:3000/users`);
  }

  public getUserByName(name) {
    return this.http.get(`http://localhost:3000/user/username`, {params: { username: name}});
  }

  public addUser(user) {
    return this.http.post(`http://localhost:3000/users/`, user);
  }

  public addFriend(user) {
    return this.http.post(`http://localhost:3000/user/${user.id}/friends`, {});
  }

  public removeFriend(user) {
    return this.http.delete(`http://localhost:3000/user/${user.id}/friends`, {});
  }

  public banUser(user) {
    return this.http.post(`http://localhost:3000/user/${user.id}/ban`, {});
  }

  public unbanUser(user) {
    return this.http.delete(`http://localhost:3000/user/${user.id}/ban`, {});
  }

  public updateUser(user, id) {
    return this.http.put(`http://localhost:3000/users/${id}`, user)
    .pipe(
      catchError(err => {
        this.flashMessagesService.show(`${err.error.error}`, { cssClass: 'alert-danger' });
        return throwError(err);
      }),
      map(() => this.flashMessagesService.show('updated', { cssClass: 'alert-success' }))
    );
  }

  public upload(f, id, avatarUrl) {
    const uploadData = new FormData();
    uploadData.append('file', f);
    uploadData.append('oldAvatarUrl', avatarUrl);

    return this.http.post(`http://localhost:3000/avatar/${id}`, uploadData);
  }
}
