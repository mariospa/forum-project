import { FlashMessagesService, FlashMessagesModule } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import { JwtModule, JwtHelperService } from '@auth0/angular-jwt';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { CONFIG } from 'src/app/config/config';
import { of } from 'rxjs';

describe('AuthService', () => {
    let httpClient;
    let router;
    let jwtService;
    let flashMessagesService;

    let service: AuthService;

    beforeEach(async(() => {
        jest.clearAllMocks();

        httpClient = {
            post() { },
            delete() { }
        };

        jwtService = {
            isTokenExpired() { },
            decodeToken() { }
        };

        router = {
            navigate() { }
        };

        flashMessagesService = {
            show() { },
        }

        TestBed.configureTestingModule({
            imports: [
                HttpClientModule,
                JwtModule.forRoot({ config: {} }),
                FlashMessagesModule],
            providers: [AuthService]
        })
            .overrideProvider(HttpClient, { useValue: httpClient })
            .overrideProvider(JwtHelperService, { useValue: jwtService })
            .overrideProvider(Router, { useValue: router })
            .overrideProvider(FlashMessagesService, { useValue: flashMessagesService });
        
            service = TestBed.get(AuthService);
    }));

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(service).toBeDefined();
      });

});
