import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Vote } from './../common/vote';
import { Observable } from 'rxjs';
import { CONFIG } from './../config/config';



@Injectable({
  providedIn: 'root',
})
export class CommentsService {
  constructor(private readonly http: HttpClient) {}

  public all(postId: number, searchOptions) {
    const options = {};
    Object.assign(options,
      searchOptions.limit && { limit: searchOptions.limit },
      searchOptions.skip && { skip: searchOptions.skip },
      searchOptions.title && { title: searchOptions.title },
      searchOptions.content && { content: searchOptions.content },
      searchOptions.deleted && { deleted: searchOptions.deleted },
    );

    return this.http.get(`${CONFIG.DOMAIN_NAME}/posts/${postId}/comments`, {
      params: {
        ...options,
      },
    });
  }

  public countComments(id, searchOptions): Observable<number> {
    const options = {};

    Object.assign(options,
      searchOptions.deleted && { deleted: searchOptions.deleted },
    );

    return this.http.get<number>(`${CONFIG.DOMAIN_NAME}/posts/${id}/comments/count`, {
      params: {
        ...options,
      }
    });
  }

  public createComment(content, postId) {
    return this.http.post(`http://localhost:3000/posts/${postId}/comments`, content);
  }

  public updateComment(content) {
    return this.http.put(`http://localhost:3000/posts/${content.postId}/comments`, content);
  }

  public deleteComment(content) {
    // console.log(content)
    return this.http.delete(`http://localhost:3000/posts/${content.postId}/comments/${content.id}`);
  }

  public likeComment(idP, idC): Observable<Vote> {
    return this.http.put<Vote>(`${CONFIG.DOMAIN_NAME}/posts/${idP}/comments/${idC}/like`, {});
  }
}
