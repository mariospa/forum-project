import { AppComponent } from '../app.component';
import { UserService } from './user.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, throwError, ObjectUnsubscribedError, BehaviorSubject } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';
import { User } from '../common/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  result;
  message;
  options;
  userLoggedIn = false;
  token;
  helper = new JwtHelperService();

  private readonly isAdminSubject$ = new BehaviorSubject<boolean>(this.isAdmin());
  private readonly isLoggedInSubject$ = new BehaviorSubject<boolean>(this.loggedIn());
  private readonly loggedUserSubject$ = new BehaviorSubject<User>(this.loggedUser());

  constructor(
    private readonly router: Router,
    private readonly userService: UserService,
    private readonly http: HttpClient,
    private flashMessagesService: FlashMessagesService,
  ) { }

  // add return type Observable<Token>, .post<Token>
  public loginUser(user) {
    return this.http.post(`http://localhost:3000/login`, user)
      .pipe(
        catchError(err => {
          this.flashMessagesService.show(`${err.error.error}`, { cssClass: 'alert-danger' });
          return throwError(err);
        }),
        map((res: any) => {
          localStorage.setItem('token', res.token);
          localStorage.setItem('user', user.username);
          this.isLoggedInSubject$.next(true);
          this.isAdminSubject$.next(this.isAdmin());
          this.loggedUserSubject$.next(this.loggedUser());
          this.flashMessagesService.show('You are logged in!', { cssClass: 'alert-success' });

        }),
      );
  }

  loggedIn() {
    const token = localStorage.getItem('token');

    return !this.helper.isTokenExpired(token);
  }

  userName() {
    return localStorage.getItem('user');
  }

  getToken() {
    return localStorage.getItem('token');
  }

  public logoutUser() {
    this.http.post(`http://localhost:3000/logout`, {}).subscribe(
      (res) => {
        this.message = res;
        if (this.message.msg === 'Successful logout!') {
          localStorage.clear();
          this.isLoggedInSubject$.next(false);
          this.isAdminSubject$.next(this.isAdmin());
          this.loggedUserSubject$.next(this.loggedUser());
          this.flashMessagesService.show(`${this.message.msg}`, { cssClass: 'alert-info' });
          this.router.navigate(['/']);

          return this.message.msg;
        }
      },
      (err) => {
        this.message = err;
        this.flashMessagesService.show(`${this.message.msg}`, { cssClass: 'alert-danger' });

        return this.message.msg;
      }
    );
  }

  private loggedUser(): User {
    try {
      return this.helper.decodeToken(localStorage.getItem('token'));
    } catch (error) {
      // in case of storage tampering
      this.isLoggedInSubject$.next(false);

      return null;
    }
  }

  private isAdmin(): boolean {
    try {
      const user = this.helper.decodeToken(localStorage.getItem('token'));
      if (user.roles.includes('Admin')) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      // in case of storage tampering
      return false;
    }
  }

  public get isLoggedIn$(): Observable<boolean> {
    return this.isLoggedInSubject$.asObservable();
  }

  public get isAdmin$(): Observable<boolean> {
    return this.isAdminSubject$.asObservable();
  }

  public get loggedUser$(): Observable<User> {
    return this.loggedUserSubject$.asObservable();
  }
}
